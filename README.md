# Install dependencies

`npm i`

# Run the dev app

Run the overlay (sidebar, grid, settings, favs):

`npm run overlay`

Run the app:

`npm run app`

# Package the app

`npm run pack`

The .exe will be in the dist directory.

# Config

- Go to https://dev.twitch.tv/console
  - Register an app
  - Put anything (like http://localhost) as OAuth url callback (not used in this app)
  - Manage the app and generate a secret
- Run the app a first time to generate the config file (dev or packaged)
- Open the settings and set the App ID and App Secret you just generated

It should generate a token and load the favorites.
