import React from 'react';
import dayjs from 'dayjs';
import { DndProvider } from 'react-dnd';
import duration from 'dayjs/plugin/duration';
import { useRoot } from 'baobab-react/hooks';
import { createRoot } from 'react-dom/client';
import { HTML5Backend } from 'react-dnd-html5-backend';

import tree from './core/tree';
import socket from './core/socket';

import Layout from './components/Layout';

const App = () => {
  const Root = useRoot(tree);

  return (
    <Root>
      <DndProvider backend={ HTML5Backend }>
        <Layout />
      </DndProvider>
    </Root>
  );
};
const root = createRoot(document.querySelector('#root'));

root.render(<App />);

dayjs.extend(duration);

window.app = {
  tree,
  socket,
};
