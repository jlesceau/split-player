import dayjs from 'dayjs';
import cls from 'classnames';
import { FaLink } from 'react-icons/fa';
import React, { useState, useEffect } from 'react';

import actions from '../../core/actions';
import { formatViewers } from '../../utils/format';

import Flag from '../Various/Flag';

function Item({ item, displayFavorites }) {
  const {
    id, type, name, channel, favName, avatar,
    isLive, title, viewers, lang, game, startedAt,
  } = item;


  /**
   * Component State
   */
  const [click, setClick] = useState(false);


  /**
   * Handlers
   */
  const makeBubble = () => {
    if (type === 'url') {
      return id;
    }

    if (!isLive) {
      return undefined;
    }

    const bubble = [title];

    if (startedAt) {
      const date = dayjs(startedAt);
      const dateDiff = dayjs
        .duration(dayjs().diff(date))
        .format('HH:mm');

      bubble.push(`\nGame: ${ game }`);
      bubble.push(`Started at: ${ date.format('HH:mm') }`);
      bubble.push(`Duration: ${ dateDiff }`);
    }

    return bubble.join('\n');
  };
  const mouseDown = () => setClick(true);
  const clearClick = () => setClick(false);
  const selectFavorite = ({ mmb, rmb, ctrl }) => {
    if (!click) return;

    if (displayFavorites === true && !(ctrl || rmb)) {
      actions.addFrame({
        newWindow: mmb,
        url: type === 'url' ? id : undefined,
        channel: type === 'url' ? undefined : channel,
      });
      actions.closeFavorites();
    } else if (displayFavorites === true) {
      actions.openSelector({
        url: type === 'url' ? id : undefined,
        channel: type === 'url' ? undefined : channel,
      });
    } else {
      actions.updateFrame({
        fid: displayFavorites,
        url: type === 'url' ? id : undefined,
        channel: type === 'url' ? undefined : channel,
      });
      actions.closeFavorites();
    }

    setClick(false);
  };


  /**
   * Lifecycle
   */
  useEffect(() => {
    window.addEventListener('mouseup', clearClick, false);

    return () => {
      window.removeEventListener('mouseup', clearClick, false);
    };
  }, []);


  /**
   * Sub renders
   */
  const renderURL = () => (
    <>
      <FaLink className="avatar div-icon url" />
      <span className="name">{ favName }</span>
      <span className="viewers" />
    </>
  );
  const renderChannel = () => (
    <>
      <img
        className="avatar"
        src={ avatar }
      />

      <span className="name">{ name || favName }</span>

      {
        isLive ?
          <Flag
            className="lang"
            lang={ lang }
          /> :
          undefined
      }

      <span className="viewers">{ formatViewers(viewers) }</span>
    </>
  );


  /**
   * Render
   */
  return (
    <button
      type="button"
      className={ cls('fav', { isLive: type === 'url' || isLive }) }
      data-for="fav-tooltip"
      title={ makeBubble() }
      onMouseDown={
        e => {
          mouseDown();

          if (e.button === 1) {
            e.preventDefault();

            return false;
          }
        }
      }
      onMouseUp={
        e => selectFavorite({
          mmb: e.button === 1,
          rmb: e.button === 2,
          ctrl: e.ctrlKey,
        })
      }
    >{
      type === 'url' ?
        renderURL() :
        renderChannel()
    }</button>
  );
}

export default Item;
