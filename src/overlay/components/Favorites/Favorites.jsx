import cls from 'classnames';
import React, { useState } from 'react';
import { TiDelete } from 'react-icons/ti';
import { useBranch } from 'baobab-react/hooks';

import actions from '../../core/actions';

import Item from './Favorites.Item';

function Favorites({ sideBarWidth }) {
  /**
   * Component State
   */
  const [tab, setTab] = useState('favorites');


  /**
   * Baobab State
   */
  const {
    tops, loading, favorites, displayFavorites,
  } = useBranch({
    favorites: ['favorites'],
    tops: ['request', 'tops'],
    loading: ['loadings', 'tops'],
    displayFavorites: ['displayFavorites'],
  });


  /**
   * Handlers
   */
  const closeFavorites = () => actions.closeFavorites();
  const changeTab = t => {
    setTab(t);

    if (t === 'tops') {
      actions.refreshTopsLive();
    }
  };


  /**
   * Render
   */
  const data = {
    tops,
    favorites,
  };

  return (
    <div
      className="favorites"
      style={{
        left: `${ sideBarWidth }px`,
      }}
    >
      <button
        type="button"
        className="close btn-icon btn-icon-red"
        onClick={ () => closeFavorites() }
      >
        <TiDelete title="Close the favorites" />
      </button>

      <div className="tabs">{
        [
          { id: 'favorites', label: 'Favorites' },
          { id: 'tops', label: 'Tops Twitch' },
        ].map(({ id, label }) => (
          <button
            key={ id }
            className={ cls('tab', { active: tab === id }) }
            type="button"
            onClick={ () => changeTab(id) }
          >{ label }</button>
        ))
      }</div>

      <div className="favorites-wrapper">
        <div className="favorites-list">{
          (data[tab] || []).map(({ id, groupName, title, channels }) => (
            <div
              key={ `${ id }-${ groupName }-${ title }` }
              className="fav-group"
            >
              <h1 className="fav-group-title">{
                title || groupName
              }</h1>
              <div className="fav-group-channels">{
                channels.map(channel => (
                  <Item
                    key={ channel.id }
                    item={ channel }
                    displayFavorites={ displayFavorites }
                  />
                ))
              }</div>
            </div>
          ))
        }</div>

        {
          loading ?
            <div className="loading-wrapper">
              <div className="loading" />
            </div> :
            undefined
        }
      </div>
    </div>
  );
}

export default Favorites;
