import React from 'react';

const flagsUrl =
  'https://raw.githubusercontent.com/lipis/flag-icon-css/master/flags/4x3';
const langsDict = {
  en: `${ flagsUrl }/gb.svg`,
  es: `${ flagsUrl }/es.svg`,
  fr: `${ flagsUrl }/fr.svg`,
  de: `${ flagsUrl }/de.svg`,
  da: `${ flagsUrl }/dk.svg`,
  el: `${ flagsUrl }/gr.svg`,
  et: `${ flagsUrl }/ee.svg`,
  fi: `${ flagsUrl }/fi.svg`,
  it: `${ flagsUrl }/it.svg`,
  ja: `${ flagsUrl }/jp.svg`,
  nl: `${ flagsUrl }/nl.svg`,
  ko: `${ flagsUrl }/kr.svg`,
};

function Flag({ lang, className }) {
  return (
    <img
      className={ className }
      src={ langsDict[lang] || `${ flagsUrl }/${ lang }.svg` }
      title={ lang.toUpperCase() }
      alt={ lang.toUpperCase() }
    />
  );
}

export default Flag;
