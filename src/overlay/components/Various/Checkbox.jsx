import React from 'react';
import cls from 'classnames';
import { GoX, GoCheck } from 'react-icons/go';

function Checkbox({ checked, onChange }) {
  return (
    <div
      className={
        cls({
          checked,
          checkbox: true,
          disabled: !onChange,
        })
      }
      onClick={
        onChange ?
          () => onChange(!checked) :
          undefined
      }
    >
      <div className="checkbox-icon">{
        checked ?
          <GoCheck /> :
          <GoX />
      }</div>

      <div className="checkbox-handle" />
    </div>
  );
}

export default Checkbox;
