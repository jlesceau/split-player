import React from 'react';
import { useBranch } from 'baobab-react/hooks';

import Grid from './Grid/Grid';
import Frames from './Frames/Frames';
import SideBar from './SideBar/SideBar';
import Settings from './Settings/Settings';
import Selector from './Selector/Selector';
import Favorites from './Favorites/Favorites';

function Layout() {
  /**
   * Baobab State
   */
  const {
    displaySidebar, sideBarWidth,
    displayGrid, displayFavorites, displaySettings, displaySelector,
  } = useBranch({
    displaySidebar: ['displaySidebar'],
    displayGrid: ['displayGrid'],
    displaySettings: ['displaySettings'],
    displayFavorites: ['displayFavorites'],
    displaySelector: ['displaySelector'],
    sideBarWidth: ['config', 'sideBarWidth'],
  });


  /**
   * Render
   */
  const sideWidth = displaySidebar ? sideBarWidth : 0;

  return (
    <div className="layout">
      {
        displaySidebar ?
          <SideBar /> :
          undefined
      }

      {
        displayGrid ?
          <Grid sideBarWidth={ sideWidth } /> :
          undefined
      }

      {
        displayGrid ?
          <Frames sideBarWidth={ sideWidth } /> :
          undefined
      }

      {
        displaySettings ?
          <Settings sideBarWidth={ sideWidth } /> :
          undefined
      }

      {
        displayFavorites ?
          <Favorites sideBarWidth={ sideWidth } /> :
          undefined
      }

      {
        displaySelector ?
          <Selector sideBarWidth={ sideWidth } /> :
          undefined
      }
    </div>
  );
}

export default Layout;
