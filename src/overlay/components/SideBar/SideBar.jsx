import React from 'react';
import { useBranch } from 'baobab-react/hooks';
import { RiLayoutMasonryFill } from 'react-icons/ri';
import { MdSettings, MdPeople } from 'react-icons/md';

import actions from '../../core/actions';

import Item from './SideBar.Item';

function SideBar() {
  /**
   * Baobab State
   */
  const { sideBarWidth, favorites } = useBranch({
    favorites: ['favorites'],
    sideBarWidth: ['config', 'sideBarWidth'],
  });


  /**
   * Handlers
   */
  const toggleGrid = () => actions.toggleGrid();
  const toggleSettings = () => actions.toggleSettings();
  const toggleFavorites = () => actions.toggleFavorites();


  /**
   * Render
   */
  const categories = favorites.filter(({ channels }) => (
    channels.some(channel => (
      channel.displayInSideBar
      && (channel.type === 'url' || channel.isLive)
    ))
  ));

  return (
    <div
      className="sidebar"
      style={{
        width: `${ sideBarWidth }px`,
      }}
    >
      <div className="buttons">
        <button
          type="button"
          className="btn-icon"
          onClick={ () => toggleGrid() }
        >
          <RiLayoutMasonryFill title="Display the grid" />
        </button>

        <button
          type="button"
          className="btn-icon"
          onClick={ () => toggleSettings() }
        >
          <MdSettings title="Open the settings" />
        </button>

        <button
          type="button"
          className="btn-icon"
          onClick={ () => toggleFavorites() }
        >
          <MdPeople title="Open the favorites" />
        </button>
      </div>

      <div className="live">{
        categories.map(({ groupName, channels }) => (
          <div
            key={ groupName }
            className="live-channel-group"
          >
            <div className="live-channel-group-title">{ groupName }</div>
            <div className="live-channel-group-channels">{
              channels
                .filter(({ type, isLive, displayInSideBar }) => (
                  displayInSideBar
                  && (type === 'url' || isLive)
                ))
                .map(channel => (
                  <Item
                    key={ channel.id }
                    item={ channel }
                  />
                ))
            }</div>
          </div>
        ))
      }</div>
    </div>
  );
}

export default SideBar;
