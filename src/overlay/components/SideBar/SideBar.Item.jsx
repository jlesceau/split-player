import dayjs from 'dayjs';
import { useDrag } from 'react-dnd';
import { FaLink } from 'react-icons/fa';
import React, { useState, useEffect } from 'react';

import actions from '../../core/actions';
import { formatViewers } from '../../utils/format';

function Item({ item }) {
  const {
    id, type, name, channel, favName, avatar,
    title, game, viewers, startedAt,
  } = item;


  /**
   * Component State
   */
  const [click, setClick] = useState(false);


  /**
   * DnD
   */
  const [, drag] = useDrag(() => ({
    type: 'OPENFRAME',
    item: () => {
      actions.openGrid();

      return (
        type === 'url' ?
          { url: id } :
          { channel }
      );
    },
    end: (it, monitor) => {
      const dropped = monitor.getDropResult();

      if (dropped) {
        actions.updateFrame({
          ...dropped,
        });
        actions.closeGrid();
      }
    },
  }));


  /**
   * Handlers
   */
  const makeBubble = () => {
    if (type === 'url') {
      return id;
    }

    const bubble = [title];

    if (startedAt) {
      const date = dayjs(startedAt);
      const dateDiff = dayjs
        .duration(dayjs().diff(date))
        .format('HH:mm');

      bubble.push(`\nGame: ${ game }`);
      bubble.push(`Started at: ${ date.format('HH:mm') }`);
      bubble.push(`Duration: ${ dateDiff }`);
    }

    return bubble.join('\n');
  };
  const mouseDown = () => setClick(true);
  const clearClick = () => setClick(false);
  const openFrame = ({ mmb, rmb, ctrl }) => {
    if (!click) return;

    if (ctrl || rmb) {
      actions.openSelector({
        url: type === 'url' ? id : undefined,
        channel: type === 'url' ? undefined : channel,
      });
    } else {
      actions.addFrame({
        newWindow: mmb,
        url: type === 'url' ? id : undefined,
        channel: type === 'url' ? undefined : channel,
      });
    }

    setClick(false);
  };


  /**
   * Lifecycle
   */
  useEffect(() => {
    window.addEventListener('mouseup', clearClick, false);

    return () => {
      window.removeEventListener('mouseup', clearClick, false);
    };
  }, []);


  /**
   * Sub renders
   */
  const renderURL = () => (
    <div className="col">
      <div className="row">
        <FaLink className="avatar div-icon url" />
        <div className="col">
          <div className="name">{ favName }</div>
        </div>
      </div>
    </div>
  );
  const renderChannel = () => (
    <div className="col">
      <div className="row">
        <img
          className="avatar"
          src={ avatar }
        />
        <div className="col">
          <div className="name">{ name }</div>
          <div className="row">
            <div className="game">{ game }</div>
            <span className="viewers">{ formatViewers(viewers) }</span>
          </div>
        </div>
      </div>
    </div>
  );


  /**
   * Render
   */
  return (
    <div
      ref={ drag }
      className="live-channel"
      title={ makeBubble() }
      onMouseDown={
        e => {
          mouseDown();

          if (e.button === 1) {
            e.preventDefault();

            return false;
          }
        }
      }
      onMouseUp={
        e => openFrame({
          mmb: e.button === 1,
          rmb: e.button === 2,
          ctrl: e.ctrlKey,
        })
      }
    >{
      type === 'url' ?
        renderURL() :
        renderChannel()
    }</div>
  );
}

export default Item;
