import cls from 'classnames';
import { TiDelete } from 'react-icons/ti';
import { BsArrowLeft } from 'react-icons/bs';
import { useBranch } from 'baobab-react/hooks';
import React, { useState, useEffect } from 'react';

import actions from '../../core/actions';

const layouts = [
  {
    id: 1,
    rows: [
      [{ code: 1 }, { code: 2 }],
    ],
  },
  {
    id: 2,
    rows: [
      [{ code: 1 }],
      [{ code: 2 }],
    ],
  },
  {
    id: 3,
    rows: [
      [{ code: 1 }, { code: 2 }],
      [{ code: 3 }, { code: 4 }],
    ],
  },
  {
    id: 4,
    rows: [
      [{ code: 1 }, { code: 2 }, { code: 3 }],
      [{ code: 4 }, { code: 5 }, { code: 6 }],
    ],
  },
];

function Selector({ sideBarWidth }) {
  /**
   * Component State
   */
  const [layout, setLayout] = useState(null);


  /**
   * Baobab State
   */
  const { displaySelector } = useBranch({
    displaySelector: ['displaySelector'],
  });


  /**
   * Handlers
   */
  const closeSelector = () => actions.closeSelector();
  const select = (lay, row, col) => {
    const { url } = displaySelector;

    actions.addFrame({
      url,
      row,
      col,
      rowCount: lay.rows.length,
      colCount: lay.rows[0].length,
    });
    closeSelector();
  };
  const keyup = e => {
    e.preventDefault();

    const esc = e.code === 'Escape';
    const capturedCode = e.code.match(/(Digit|Numpad)(\d)/)[2];

    if (esc && layout) {
      setLayout(null);
    } else if (capturedCode && !layout) {
      layouts.forEach(lay => {
        if (lay.id === +capturedCode) {
          setLayout(lay);
        }
      });
    } else if (capturedCode) {
      layout.rows.forEach((row, i) => {
        row.forEach(({ code }, j) => {
          if (code === +capturedCode) {
            select(layout, i, j);
          }
        });
      });
    }
  };


  /**
   * Lifecycle
   */
  useEffect(() => {
    window.addEventListener('keyup', keyup, false);

    return () => {
      window.removeEventListener('keyup', keyup, false);
    };
  });


  /**
   * Render
   */
  return (
    <div
      className="selector"
      style={{
        left: `${ sideBarWidth }px`,
      }}
    >
      <button
        type="button"
        className="close btn-icon btn-icon-red"
        onClick={ () => closeSelector() }
      >
        <TiDelete title="Cancel" />
      </button>

      <div className="selector-title">
        {
          layout ?
            <button
              type="button"
              className="back btn-icon"
              onClick={ () => setLayout(null) }
            >
              <BsArrowLeft title="Back to layouts" />
            </button> :
            <div className="back" />
        }

        <div>{
          layout ?
            'Select a position' :
            'Select a layout'
        }</div>
      </div>

      <div className="selector-wrapper">{
        layouts.map(lay => (
          <div
            key={ lay.id }
            className={
              cls({
                'layout-choice': true,
                chosen: layout && layout.id === lay.id,
                hidden: layout && layout.id !== lay.id,
              })
            }
          >
            {
              !layout ?
                <div className="keyboard-key">
                  <div className="code">{ lay.id }</div>
                </div> :
                undefined
            }

            {
              lay.rows.map((row, i) => (
                <div
                  key={ i }
                  className="layout-row"
                >{
                  row
                    .reduce((list, rect, j) => ([
                      ...list,
                      { ...rect, position: j },
                      {},
                    ]), [])
                    .map(({ position, code }, k) => (
                      position !== undefined ?
                        <div
                          key={ position }
                          className="rect"
                          onClick={ () => select(lay, i, position) }
                        >{
                          layout ?
                            <div className="keyboard-key">
                              <div className="code">{ code }</div>
                            </div> :
                            undefined
                        }</div> :
                        <div
                          key={ `bar-${ k }` }
                          className="bar"
                        />
                    ))
                }</div>
              ))
            }
          </div>
        ))
      }</div>
    </div>
  );
}

export default Selector;
