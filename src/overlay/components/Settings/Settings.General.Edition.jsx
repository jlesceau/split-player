import React from 'react';
import cls from 'classnames';

import meta from '../../assets/settings-edition.json';

import Checkbox from '../Various/Checkbox';

function Edition({ edition, setEdition, cancel, save }) {
  /**
   * Render
   */
  const renderTextarea = () => (
    <textarea
      value={ edition.value }
      onChange={
        e => setEdition({
          ...edition,
          value: e.target.value,
        })
      }
    />
  );
  const renderCheckbox = () => (
    <div className="fixed-width-xs">
      <Checkbox
        checked={ edition.value }
        onChange={
          checked => setEdition({
            ...edition,
            value: checked,
          })
        }
      />
    </div>
  );
  const renderInput = () => (
    <input
      className="fixed-width-xl"
      type={ meta[edition.id].type }
      value={ edition.value }
      min={ meta[edition.id].min }
      max={ meta[edition.id].max }
      step={ meta[edition.id].step }
      onChange={
        e => setEdition({
          ...edition,
          value: e.target.value,
        })
      }
      required
    />
  );
  const renderers = {
    textarea: renderTextarea,
    checkbox: renderCheckbox,
  };

  return (
    <div className="settings-edit">
      <form
        className="settings-block"
        onSubmit={
          e => {
            e.preventDefault();
            save();
          }
        }
        onReset={ () => cancel() }
      >
        <h1>{ meta[edition.id].label }</h1>

        <div className="settings-line">
          <div>{ meta[edition.id].description }</div>
        </div>

        <div
          className={
            cls({
              'settings-line': true,
              'settings-line-textarea':
                meta[edition.id].type === 'textarea',
            })
          }
        >{
          renderers[meta[edition.id].type] ?
            renderers[meta[edition.id].type]() :
            renderInput()
        }</div>

        <div className="settings-line settings-line-buttons">
          <button
            type="reset"
            className="btn-text"
          >Cancel</button>
          <button
            type="submit"
            className="btn-text"
          >Validate</button>
        </div>
      </form>
    </div>
  );
}

export default Edition;
