import { clamp } from 'lodash';
import React, { useState } from 'react';
import { useBranch } from 'baobab-react/hooks';
import { MdHelpOutline, MdOutlineEdit } from 'react-icons/md';

import actions from '../../core/actions';

import meta from '../../assets/settings-edition.json';

import Checkbox from '../Various/Checkbox';
import Edition from './Settings.General.Edition';

function SettingsGeneral() {
  /**
   * Component State
   */
  const [edition, setEdition] = useState(null);


  /**
   * Baobab State
   */
  const { config } = useBranch({
    config: ['config'],
  });


  /**
   * Handlers
   */
  const getValue = (id, boolean) => {
    const path = id.split('.');
    const v = path.reduce((value, key) => value[key], config);

    if (boolean) {
      return <Checkbox checked={ v } />;
    }

    return v;
  };
  const cancelEdition = () => setEdition(null);
  const saveSettings = () => {
    if (!edition) return;

    let { value } = edition;

    if (meta[edition.id].type === 'number') {
      const min = 'min' in meta[edition.id] ?
        meta[edition.id].min :
        -Infinity;
      const max = 'max' in meta[edition.id] ?
        meta[edition.id].max :
        Infinity;

      value = clamp(+value, min, max);
    }

    if (['extensions', 'VIPs'].includes(edition.id)) {
      value = value
        .split(',')
        .map(v => v.trim());
    }

    actions.editConfig({
      key: edition.id,
      value,
    });
    setEdition(null);
  };


  /**
   * Render
   */
  return (
    <div className="settings-body">
      <div className="settings-body-wrapper">
        <div className="settings-list">
          {
            [
              {
                title: 'Twitch API Credentials',
                ids: [
                  'credentials.authUrl',
                  'credentials.url',
                  'credentials.appId',
                  'credentials.appSecret',
                  'credentials.token',
                ],
              },
              {
                title: 'Window position at launch',
                ids: [
                  'position.top',
                  'position.left',
                ],
              },
              {
                title: 'Grid size',
                ids: [
                  'grid.height',
                  'grid.width',
                ],
              },
              {
                title: 'Side bar',
                ids: [
                  'sideBarWidth',
                ],
              },
              {
                title: 'Tops Twitch',
                ids: [
                  'tops.top',
                ],
              },
              {
                title: 'Favorites',
                ids: [
                  'channelsRefreshRate',
                  'cacheExpiration',
                ],
              },
              {
                title: 'Adblocker',
                ids: [
                  'useAdblocker',
                ],
              },
            ].map(({ title, ids }) => (
              <div
                key={ title }
                className="settings-block"
              >
                <h1>{ title }</h1>

                <div>{
                  ids.map(id => (
                    <div
                      key={ id }
                      className="settings-line"
                    >
                      <div
                        className="fixed-width-m"
                      >{ meta[id].label }</div>
                      <div
                        className="div-icon"
                        title={ meta[id].description }
                      >
                        <MdHelpOutline />
                      </div>
                      <div className="flex-width">{
                        getValue(id, meta[id].type === 'checkbox')
                      }</div>
                      {
                        !meta[id].readonly ?
                          <button
                            type="button"
                            className="btn-icon"
                            onClick={
                              () => setEdition({
                                id,
                                value: getValue(id),
                              })
                            }
                          >
                            <MdOutlineEdit title="Edit the setting" />
                          </button> :
                          undefined
                      }
                    </div>
                  ))
                }</div>
              </div>
            ))
          }

          {
            [
              { title: 'Extensions', id: 'extensions' },
              { title: 'Twitch VIPs', id: 'VIPs' },
            ].map(({ title, id }) => (
              <div
                key={ title }
                className="settings-block"
              >
                <h1>
                  <span>{ title }</span>
                  <button
                    type="button"
                    className="btn-icon"
                    onClick={
                      () => setEdition({
                        id,
                        value: getValue(id).join(', '),
                      })
                    }
                  >
                    <MdOutlineEdit title="Edit the setting" />
                  </button>
                </h1>

                <div className={ title }>{
                  getValue(id).map(v => <div key={ v }>{ v }</div>)
                }</div>
              </div>
            ))
          }
        </div>

        {
          edition ?
            <Edition
              edition={ edition }
              setEdition={ setEdition }
              cancel={ cancelEdition }
              save={ saveSettings }
            /> :
            undefined
        }
      </div>
    </div>
  );
}

export default SettingsGeneral;
