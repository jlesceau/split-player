import React from 'react';
import { TiDelete } from 'react-icons/ti';
import { useBranch } from 'baobab-react/hooks';

import actions from '../../core/actions';

function Edition({ edition, setEdition, cancel, save }) {
  /**
   * Baobab State
   */
  const { request } = useBranch({
    request: ['request'],
  });


  /**
   * Handlers
   */
  const searchCategory = query => {
    actions.requestCategory({ query });
  };
  const selectCategory = id => {
    setEdition({
      ...edition,
      id,
    });
    actions.clearRequests();
  };


  /**
   * Render
   */
  return (
    <div className="settings-edit">
      <form
        className="settings-block"
        onSubmit={
          e => {
            e.preventDefault();
            save();
          }
        }
        onReset={ () => cancel() }
      >
        <h1>Edit a Category</h1>

        <div className="settings-line">
          <label
            htmlFor="title"
            className="fixed-width-m"
          >Category name</label>
          <input
            id="title"
            className="fixed-width-xl"
            type="text"
            value={ edition.title }
            onChange={
              e => setEdition({
                ...edition,
                title: e.target.value,
              })
            }
            required
          />
        </div>

        <div className="settings-line">
          <label
            htmlFor="lang"
            className="fixed-width-m"
          >Language (optional)</label>
          <input
            id="lang"
            className="fixed-width-xl"
            type="text"
            value={ edition.lang }
            onChange={
              e => setEdition({
                ...edition,
                lang: e.target.value,
              })
            }
          />
        </div>

        <div className="settings-line">
          <label
            htmlFor="catId"
            className="fixed-width-m"
          >Category ID</label>
          <input
            id="catId"
            className="fixed-width-xl"
            type="text"
            value={ edition.id }
            disabled
          />
          <button
            type="button"
            className="btn-icon"
            onClick={ () => selectCategory('') }
          >
            <TiDelete title="Clear the ID" />
          </button>
        </div>

        <div className="settings-line">
          <label
            htmlFor="search"
            className="fixed-width-m"
          >Search a category</label>
          <input
            id="search"
            className="fixed-width-xl"
            type="text"
            onKeyDown={
              e => {
                if (e.key === 'Enter') {
                  e.preventDefault();

                  searchCategory(e.target.value);
                }
              }
            }
          />
        </div>

        {
          request.categories.length ?
            <div className="settings-results">{
              request.categories.map(category => (
                <div
                  key={ category.id }
                  className="settings-line settings-line-results"
                  onClick={ () => selectCategory(category.id) }
                >
                  <img
                    className="cover"
                    src={ category.box_art_url }
                  />
                  <div className="flex-width">{ category.name }</div>
                  <div className="fixed-width-s">{ category.id }</div>
                </div>
              ))
            }</div> :
            undefined
        }

        <div className="settings-line settings-line-buttons">
          <button
            type="reset"
            className="btn-text"
          >Cancel</button>
          <button
            type="submit"
            className="btn-text"
          >Validate</button>
        </div>
      </form>
    </div>
  );
}

export default Edition;
