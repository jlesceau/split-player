import { cloneDeep } from 'lodash';
import React, { useState } from 'react';
import { TiDelete } from 'react-icons/ti';
import { useBranch } from 'baobab-react/hooks';
import { MdOutlineEdit } from 'react-icons/md';

import actions from '../../core/actions';

import Flag from '../Various/Flag';
import Edition from './Settings.Tops.Edition';

function SettingsTops() {
  /**
   * Component State
   */
  const [editCat, setEditCat] = useState(null);


  /**
   * Baobab State
   */
  const { config } = useBranch({
    config: ['config'],
  });


  /**
   * Handlers
   */
  const setEdition = params => {
    setEditCat(params);
    actions.clearRequests();
  };
  const cancelEdition = () => setEdition(null);
  const saveCategory = () => {
    if (!editCat) return;

    const categories = cloneDeep(config.tops.categories);
    const { line, id, lang, title } = editCat;

    categories.splice(line, 1, { id, lang, title });

    actions.editConfig({
      key: 'tops.categories',
      value: categories,
    });
    setEdition(null);
  };
  const removeCategory = i => {
    const categories = cloneDeep(config.tops.categories);

    categories.splice(i, 1);

    actions.editConfig({
      key: 'tops.categories',
      value: categories,
    });
    setEdition(null);
  };


  /**
   * Render
   */
  return (
    <div className="settings-body">
      <div className="settings-body-wrapper">
        <div className="settings-list">
          <div className="settings-block">
            <h1>Categories</h1>

            {
              config.tops.categories.map(({ id, lang, title }, i) => (
                <div
                  key={ i }
                  className="settings-line"
                >
                  <div className="div-icon">{
                    lang ?
                      <Flag
                        lang={ lang }
                        className="lang"
                      /> :
                      undefined
                  }</div>
                  <div
                    className="flex-width ellipsis"
                    title={ title }
                  >{ title }</div>
                  <div
                    className="fixed-width-s ellipsis"
                    title={ id }
                  >
                    ID: { id !== '' ? id : '-' }
                  </div>
                  <button
                    type="button"
                    className="btn-icon"
                    onClick={
                      () => setEdition({
                        line: i,
                        id,
                        lang,
                        title,
                      })
                    }
                  >
                    <MdOutlineEdit title="Edit the Category" />
                  </button>
                  <button
                    type="button"
                    className="btn-icon btn-icon-red"
                    onClick={ () => removeCategory(i) }
                  >
                    <TiDelete title="Remove the Category" />
                  </button>
                </div>
              ))
            }

            <div className="settings-line settings-line-buttons">
              <button
                type="button"
                className="btn-text"
                onClick={
                  () => setEdition({
                    line: config.tops.categories.length,
                    id: '',
                    lang: '',
                    title: '',
                  })
                }
              >New category</button>
            </div>
          </div>
        </div>


        {
          editCat ?
            <Edition
              edition={ editCat }
              setEdition={ setEditCat }
              cancel={ cancelEdition }
              save={ saveCategory }
            /> :
            undefined
        }
      </div>
    </div>
  );
}

export default SettingsTops;
