import cls from 'classnames';
import { cloneDeep } from 'lodash';
import React, { useState } from 'react';
import { TiDelete } from 'react-icons/ti';
import { useBranch } from 'baobab-react/hooks';
import { RiSideBarLine } from 'react-icons/ri';
import { FaTwitch, FaLink } from 'react-icons/fa';
import {
  MdOutlineEdit,
  MdNotificationsNone,
  MdNotificationsActive,
} from 'react-icons/md';

import actions from '../../core/actions';

import EditFav from './Settings.Favorites.EditFav';
import EditGroup from './Settings.Favorites.EditGroup';

const typeIcons = {
  twitch: {
    icon: FaTwitch,
    title: 'Twitch Channel',
  },
  url: {
    icon: FaLink,
    title: 'URL',
  },
};

function SettingsFavorites() {
  /**
   * Component State
   */
  const [editGroup, setEditGroup] = useState(null);
  const [editFav, setEditFav] = useState(null);


  /**
   * Baobab State
   */
  const { config } = useBranch({
    config: ['config'],
  });


  /**
   * Handlers
   */
  const setEdition = (type, params) => {
    if (type === 'group') {
      setEditGroup(params);
      setEditFav(null);
    } else if (type === 'channel') {
      setEditFav(params);
      setEditGroup(null);
    } else {
      setEditGroup(null);
      setEditFav(null);
    }

    actions.clearRequests();
  };
  const cancelEdition = () => setEdition(null);
  const saveGroup = () => {
    if (!editGroup) return;

    const favorites = cloneDeep(config.favorites);
    const { group, name } = editGroup;

    if (group >= config.favorites.length) {
      favorites.push({ groupName: name, channels: [] });
    } else {
      favorites[group].groupName = name;
    }

    actions.editConfig({
      key: 'favorites',
      value: favorites,
    });
    setEdition(null);
  };
  const removeGroup = i => {
    const favorites = cloneDeep(config.favorites);

    favorites.splice(i, 1);

    actions.editConfig({
      key: 'favorites',
      value: favorites,
    });
    setEdition(null);
  };
  const saveFav = () => {
    if (!editFav || !editFav.id || !editFav.favName) return;

    const favorites = cloneDeep(config.favorites);
    const {
      group,
      line,
      id,
      type,
      favName,
      displayInSideBar,
      notification,
    } = editFav;

    favorites[group].channels.splice(line, 1, {
      id,
      type,
      favName,
      displayInSideBar,
      notification,
    });

    actions.editConfig({
      key: 'favorites',
      value: favorites,
    });
    setEdition(null);
  };
  const removeChannel = (i, j) => {
    const favorites = cloneDeep(config.favorites);

    favorites[i].channels.splice(j, 1);

    actions.editConfig({
      key: 'favorites',
      value: favorites,
    });
    setEdition(null);
  };
  const toggle = (group, line, key) => {
    const favorites = cloneDeep(config.favorites);
    const fav = favorites[group].channels[line];

    favorites[group].channels.splice(line, 1, {
      ...fav,
      [key]: !fav[key],
    });

    actions.editConfig({
      key: 'favorites',
      value: favorites,
    });
  };


  /**
   * Sub renders
   */
  const renderTypeIcon = type => {
    const Icon = typeIcons[type].icon;

    return <Icon title={ typeIcons[type].title } />;
  };


  /**
   * Render
   */
  return (
    <div className="settings-body">
      <div className="settings-body-wrapper">
        <div className="settings-list">
          <h1>Favorites</h1>

          <div>{
            config.favorites.map(({ groupName, channels }, i) => (
              <div
                key={ i }
                className="settings-block"
              >
                <h2>
                  <span className="ellipsis">{ groupName }</span>
                  <button
                    type="button"
                    className="btn-icon"
                    onClick={
                      () => setEdition('group', {
                        group: i,
                        name: groupName,
                      })
                    }
                  >
                    <MdOutlineEdit title="Edit the group name" />
                  </button>
                  <button
                    type="button"
                    className="btn-icon btn-icon-red"
                    onClick={ () => removeGroup(i) }
                  >
                    <TiDelete title="Remove the group of favorites" />
                  </button>
                </h2>

                <div>{
                  channels.map((
                    { type, id, favName, displayInSideBar, notification },
                    j
                  ) => (
                    <div
                      key={ j }
                      className="settings-line"
                    >
                      <div
                        className={ cls('div-icon', type) }
                      >{
                        renderTypeIcon(type)
                      }</div>
                      <button
                        type="button"
                        className={
                          cls({
                            'btn-icon': true,
                            inactive: !displayInSideBar,
                          })
                        }
                        onClick={ () => toggle(i, j, 'displayInSideBar') }
                      >
                        <RiSideBarLine title="Display in the sidebar" />
                      </button>
                      <button
                        type="button"
                        className={
                          cls({
                            'btn-icon': true,
                            inactive: !notification,
                          })
                        }
                        onClick={ () => toggle(i, j, 'notification') }
                      >{
                        notification ?
                          <MdNotificationsActive
                            title="Notified when going live"
                          /> :
                          <MdNotificationsNone
                            title="Not notified when going live"
                          />
                      }</button>
                      <div className="flex-width ellipsis">{ favName }</div>
                      <div
                        className="fixed-width-m ellipsis"
                        title={ id }
                      >{ id }</div>
                      <button
                        type="button"
                        className="btn-icon"
                        onClick={
                          () => setEdition('channel', {
                            group: i,
                            line: j,
                            id,
                            type,
                            favName,
                            displayInSideBar,
                            notification,
                          })
                        }
                      >
                        <MdOutlineEdit title="Edit the favorite" />
                      </button>
                      <button
                        type="button"
                        className="btn-icon btn-icon-red"
                        onClick={ () => removeChannel(i, j) }
                      >
                        <TiDelete title="Remove the favorite" />
                      </button>
                    </div>
                  ))
                }</div>

                <div className="settings-line settings-line-buttons">
                  <button
                    type="button"
                    className="btn-text"
                    onClick={
                      () => setEdition('channel', {
                        group: i,
                        line: channels.length,
                        id: '',
                        type: 'twitch',
                        favName: '',
                        displayInSideBar: true,
                        notification: false,
                      })
                    }
                  >New channel</button>
                </div>
              </div>
            ))
          }</div>

          <div className="settings-block">
            <div className="settings-line settings-line-buttons">
              <button
                type="button"
                className="btn-text"
                onClick={
                  () => setEdition('group', {
                    group: config.favorites.length,
                    name: '',
                  })
                }
              >New group</button>
            </div>
          </div>
        </div>

        {
          editGroup ?
            <EditGroup
              edition={ editGroup }
              setEdition={ setEditGroup }
              cancel={ cancelEdition }
              save={ saveGroup }
            /> :
            undefined
        }

        {
          editFav ?
            <EditFav
              edition={ editFav }
              setEdition={ setEditFav }
              cancel={ cancelEdition }
              save={ saveFav }
            /> :
            undefined
        }
      </div>
    </div>
  );
}

export default SettingsFavorites;
