import React from 'react';

function Edition({ edition, setEdition, cancel, save }) {
  /**
   * Render
   */
  return (
    <div className="settings-edit">
      <form
        className="settings-block"
        onSubmit={
          e => {
            e.preventDefault();
            save();
          }
        }
        onReset={ () => cancel() }
      >
        <h1>Edit a Script</h1>

        <div className="settings-line">
          <label
            htmlFor="domain"
            className="fixed-width-m"
          >Domain</label>
          <input
            id="domain"
            className="fixed-width-xl"
            type="text"
            value={ edition.domain }
            onChange={
              e => setEdition({
                ...edition,
                domain: e.target.value,
              })
            }
            required
          />
        </div>

        <div className="settings-line">
          <label
            htmlFor="js"
            className="fixed-width-m"
          >JS file name (optional)</label>
          <input
            id="js"
            className="fixed-width-xl"
            type="text"
            value={ edition.js }
            onChange={
              e => setEdition({
                ...edition,
                js: e.target.value,
              })
            }
          />
        </div>

        <div className="settings-line">
          <label
            htmlFor="css"
            className="fixed-width-m"
          >CSS file name (optional)</label>
          <input
            id="css"
            className="fixed-width-xl"
            type="text"
            value={ edition.css }
            onChange={
              e => setEdition({
                ...edition,
                css: e.target.value,
              })
            }
          />
        </div>

        <div className="settings-line settings-line-buttons">
          <button
            type="reset"
            className="btn-text"
          >Cancel</button>
          <button
            type="submit"
            className="btn-text"
          >Validate</button>
        </div>
      </form>
    </div>
  );
}

export default Edition;
