import cls from 'classnames';
import React, { useState } from 'react';
import { TiDelete } from 'react-icons/ti';

import actions from '../../core/actions';

import Tops from './Settings.Tops';
import General from './Settings.General';
import Scripts from './Settings.Scripts';
import Favorites from './Settings.Favorites';

const components = {
  general: General,
  tops: Tops,
  favorites: Favorites,
  scripts: Scripts,
};

function Settings({ sideBarWidth }) {
  /**
   * Component State
   */
  const [tab, setTab] = useState('general');


  /**
   * Handlers
   */
  const closeSettings = () => actions.closeSettings();
  const changeTab = t => {
    setTab(t);
    actions.clearRequests();
  };


  /**
   * Render
   */
  const Component = components[tab];

  return (
    <div
      className="settings"
      style={{
        left: `${ sideBarWidth }px`,
      }}
    >
      <button
        type="button"
        className="close btn-icon btn-icon-red"
        onClick={ () => closeSettings() }
      >
        <TiDelete title="Close the settings" />
      </button>

      <div className="tabs">{
        [
          { id: 'general', label: 'General' },
          { id: 'tops', label: 'Tops Twitch' },
          { id: 'favorites', label: 'Favorites' },
          { id: 'scripts', label: 'Scripts' },
        ].map(({ id, label }) => (
          <button
            key={ id }
            className={ cls('tab', { active: tab === id }) }
            type="button"
            onClick={
              () => changeTab(id)
            }
          >{ label }</button>
        ))
      }</div>

      <Component />
    </div>
  );
}

export default Settings;
