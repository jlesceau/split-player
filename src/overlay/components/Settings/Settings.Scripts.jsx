import { cloneDeep } from 'lodash';
import { TiDelete } from 'react-icons/ti';
import { useBranch } from 'baobab-react/hooks';
import { MdOutlineEdit } from 'react-icons/md';
import React, { useState, Fragment } from 'react';

import actions from '../../core/actions';

import Edition from './Settings.Scripts.Edition';

function SettingsScripts() {
  /**
   * Component State
   */
  const [editScript, setEditScript] = useState(null);


  /**
   * Baobab State
   */
  const { config } = useBranch({
    config: ['config'],
  });


  /**
   * Handlers
   */
  const cancelEdition = () => setEditScript(null);
  const saveScript = () => {
    if (!editScript) return;

    const scripts = cloneDeep(config.scripts);
    const { line, domain, js, css } = editScript;

    scripts.splice(line, 1, { domain, js, css });

    actions.editConfig({
      key: 'scripts',
      value: scripts,
    });
    setEditScript(null);
  };
  const removeScript = i => {
    const scripts = cloneDeep(config.scripts);

    scripts.splice(i, 1);

    actions.editConfig({
      key: 'scripts',
      value: scripts,
    });
    setEditScript(null);
  };


  /**
   * Render
   */
  return (
    <div className="settings-body">
      <div className="settings-body-wrapper">
        <div className="settings-list">
          <div className="settings-block">
            <h1>Scripts</h1>

            {
              config.scripts.map(({ domain, js, css }, i) => (
                <Fragment key={ `${ domain }-${ i }` }>
                  <div className="settings-line">
                    <div
                      className="flex-width ellipsis"
                      title={ domain }
                    >{ domain }</div>
                    <button
                      type="button"
                      className="btn-icon"
                      onClick={
                        () => setEditScript({
                          line: i,
                          domain,
                          js,
                          css,
                        })
                      }
                    >
                      <MdOutlineEdit title="Edit the Script" />
                    </button>
                    <button
                      type="button"
                      className="btn-icon btn-icon-red"
                      onClick={ () => removeScript(i) }
                    >
                      <TiDelete title="Remove the Script" />
                    </button>
                  </div>

                  <div className="settings-line">
                    <div className="fixed-width-xs" />
                    <div className="fixed-width-m">JS file</div>
                    <div
                      className="flex-width ellipsis"
                      title={ js }
                    >{ js }</div>
                  </div>

                  <div className="settings-line">
                    <div className="fixed-width-xs" />
                    <div className="fixed-width-m">CSS file</div>
                    <div
                      className="flex-width ellipsis"
                      title={ css }
                    >{ css }</div>
                  </div>
                </Fragment>
              ))
            }

            <div className="settings-line settings-line-buttons">
              <button
                type="button"
                className="btn-text"
                onClick={
                  () => setEditScript({
                    line: config.scripts.length,
                    domain: '',
                    js: null,
                    css: null,
                  })
                }
              >New script</button>
            </div>
          </div>
        </div>


        {
          editScript ?
            <Edition
              edition={ editScript }
              setEdition={ setEditScript }
              cancel={ cancelEdition }
              save={ saveScript }
            /> :
            undefined
        }
      </div>
    </div>
  );
}

export default SettingsScripts;
