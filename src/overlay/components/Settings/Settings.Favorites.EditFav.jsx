import cls from 'classnames';
import React from 'react';
import { TiDelete } from 'react-icons/ti';
import { useBranch } from 'baobab-react/hooks';

import actions from '../../core/actions';

import Checkbox from '../Various/Checkbox';

function EditFav({ edition, setEdition, cancel, save }) {
  /**
   * Baobab State
   */
  const { request } = useBranch({
    request: ['request'],
  });


  /**
   * Handlers
   */
  const selectType = type => {
    setEdition({
      ...edition,
      type,
      id: '',
      favName: '',
      notification: type === 'url' ? false : edition.notification,
    });
    actions.clearRequests();
  };
  const searchChannel = query => {
    actions.requestChannel({ query });
  };
  const selectChannel = (id, favName) => {
    setEdition({
      ...edition,
      id,
      favName,
    });
    actions.clearRequests();
  };


  /**
   * Render
   */
  return (
    <div className="settings-edit">
      <h1>Edit a Favorite</h1>
      <form
        className="settings-block"
        onSubmit={
          e => {
            e.preventDefault();
            save();
          }
        }
        onReset={ () => cancel() }
      >
        <div className="settings-line">
          <div className="fixed-width-m">Type of favorite</div>
          <label
            htmlFor="twitch"
            className="fixed-width-xs"
          >Twitch</label>
          <input
            id="twitch"
            name="type"
            type="radio"
            className="fixed-width-xs"
            value="twitch"
            checked={ edition.type === 'twitch' }
            onChange={ e => selectType(e.target.value) }
          />
        </div>

        <div className="settings-line">
          <div className="fixed-width-m" />
          <label
            htmlFor="url"
            className="fixed-width-xs"
          >URL</label>
          <input
            id="url"
            name="type"
            type="radio"
            className="fixed-width-xs"
            value="url"
            checked={ edition.type === 'url' }
            onChange={ e => selectType(e.target.value) }
          />
        </div>

        <div className="settings-line">
          <div className="fixed-width-m">Display in Sidebar</div>
          <div className="fixed-width-xs">
            <Checkbox
              checked={ edition.displayInSideBar }
              onChange={
                checked => setEdition({
                  ...edition,
                  displayInSideBar: checked,
                })
              }
            />
          </div>
        </div>

        {
          edition.type === 'twitch' ?
            <div className="settings-line">
              <div className="fixed-width-m">Notification when going live</div>
              <div className="fixed-width-xs">
                <Checkbox
                  checked={ edition.notification }
                  onChange={
                    checked => setEdition({
                      ...edition,
                      notification: checked,
                    })
                  }
                />
              </div>
            </div> :
            undefined
        }

        <div className="settings-line">
          <label
            htmlFor="name"
            className="fixed-width-m"
          >{
            edition.type === 'twitch' ?
              'Channel name' :
              'Title'
          }</label>
          <input
            id="name"
            className={
              cls({
                'fixed-width-xl': true,
                invalid: edition.favName === '',
              })
            }
            type="text"
            value={ edition.favName }
            onChange={
              e => setEdition({
                ...edition,
                favName: e.target.value,
              })
            }
            disabled={ edition.type === 'twitch' }
            required
          />
          {
            edition.type === 'twitch' ?
              <button
                type="button"
                className="btn-icon"
                onClick={ () => selectChannel('', '') }
              >
                <TiDelete title="Clear the channel" />
              </button> :
              undefined
          }
        </div>

        <div className="settings-line">
          <label
            htmlFor="channelId"
            className="fixed-width-m"
          >{
            edition.type === 'twitch' ?
              'Channel ID' :
              'URL'
          }</label>
          <input
            id="channelId"
            className={
              cls({
                'fixed-width-xl': true,
                invalid: edition.id === '',
              })
            }
            type="text"
            value={ edition.id }
            onChange={
              e => setEdition({
                ...edition,
                id: e.target.value,
              })
            }
            disabled={ edition.type === 'twitch' }
            required
          />
        </div>

        {
          edition.type === 'twitch' ?
            <div className="settings-line">
              <label
                htmlFor="search"
                className="fixed-width-m"
              >Search a channel</label>
              <input
                id="search"
                className="fixed-width-xl"
                type="text"
                onKeyDown={
                  e => {
                    if (e.key === 'Enter') {
                      e.preventDefault();

                      searchChannel(e.target.value);
                    }
                  }
                }
              />
            </div> :
            undefined
        }

        {
          request.channels.length ?
            <div className="settings-results">{
              request.channels.map(channel => (
                <div
                  key={ channel.id }
                  className="settings-line settings-line-results"
                  onClick={
                    () => selectChannel(channel.id, channel.display_name)
                  }
                >
                  <img
                    className="avatar"
                    src={ channel.thumbnail_url }
                  />
                  <span className="flex-width">{ channel.display_name }</span>
                  <span className="fixed-width-s">{ channel.id }</span>
                </div>
              ))
            }</div> :
            undefined
        }

        <div className="settings-line settings-line-buttons">
          <button
            type="reset"
            className="btn-text"
          >Cancel</button>
          <button
            type="submit"
            className="btn-text"
          >Validate</button>
        </div>
      </form>
    </div>
  );
}

export default EditFav;
