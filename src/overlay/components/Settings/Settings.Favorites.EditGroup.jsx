import React from 'react';

function EditGroup({ edition, setEdition, cancel, save }) {
  /**
   * Render
   */
  return (
    <div className="settings-edit">
      <h1>Edit a Group</h1>
      <form
        className="settings-block"
        onSubmit={
          e => {
            e.preventDefault();
            save();
          }
        }
        onReset={ () => cancel() }
      >
        <div className="settings-line">
          <label
            htmlFor="name"
            className="fixed-width-m"
          >Group name</label>
          <input
            id="name"
            className="fixed-width-xl"
            type="text"
            value={ edition.name }
            onChange={
              e => setEdition({
                ...edition,
                name: e.target.value,
              })
            }
            required
          />
        </div>

        <div className="settings-line settings-line-buttons">
          <button
            type="reset"
            className="btn-text"
          >Cancel</button>
          <button
            type="submit"
            className="btn-text"
          >Validate</button>
        </div>
      </form>
    </div>
  );
}

export default EditGroup;
