import React from 'react';
import cls from 'classnames';
import { useDrop } from 'react-dnd';
import { TiDelete } from 'react-icons/ti';
import { RiLayoutRightFill } from 'react-icons/ri';
import { FaTwitch, FaLink, FaArrowRight } from 'react-icons/fa';
import { MdPeople, MdRefresh, MdSwapHoriz } from 'react-icons/md';

import actions from '../../core/actions';

function Frame({ swap, frame }) {
  const { fid, url } = frame;


  /**
   * DnD
   */
  const [{ isOver }, drop] = useDrop(() => ({
    accept: 'OPENFRAME',
    drop: item => ({
      fid,
      url: item.url,
      channel: item.channel,
    }),
    collect: monitor => ({
      isOver: monitor.isOver(),
    }),
  }));


  /**
   * Handlers
   */
  const updateFrame = params => actions.updateFrame({
    fid,
    url: params.url,
    channel: params.channel,
  });
  const reloadFrame = () => actions.reloadFrame({ fid });
  const openFavorites = () => actions.openFavorites({ fid });
  const swapFrames = () => actions.swapFrames({ fid });
  const openDevTools = () => actions.openDevTools({ fid });
  const deleteFrame = () => actions.deleteFrame({ fid });


  /**
   * Render
   */
  return (
    <div
      key={ url }
      ref={ drop }
      className={ cls('frame', { dropTarget: isOver }) }
    >
      <div className="frame-ctrls">
        <form
          className="frame-ctrls-line"
          onSubmit={
            e => {
              e.preventDefault();
              updateFrame({ channel: new FormData(e.target).get('channel') });
            }
          }
        >
          <div className="div-icon">
            <FaTwitch title="Twitch" />
          </div>
          <input
            type="text"
            name="channel"
            defaultValue=""
          />
          <button
            type="submit"
            className="btn-icon btn-go"
          >
            <FaArrowRight title="Go" />
          </button>
        </form>

        <form
          className="frame-ctrls-line"
          onSubmit={
            e => {
              e.preventDefault();
              updateFrame({ url: new FormData(e.target).get('url') });
            }
          }
        >
          <div className="div-icon">
            <FaLink title="URL" />
          </div>
          <input
            type="text"
            name="url"
            defaultValue={ url }
          />
          <button
            type="submit"
            className="btn-icon btn-go"
          >
            <FaArrowRight title="Go" />
          </button>
        </form>

        <div className="frame-ctrls-line">
          <button
            type="button"
            className="btn-icon"
            onClick={ () => reloadFrame() }
          >
            <MdRefresh title="Refresh" />
          </button>

          <button
            type="button"
            className="btn-icon"
            onClick={ () => openFavorites() }
          >
            <MdPeople title="Open the favorites" />
          </button>

          <button
            type="button"
            className={ cls('btn-icon', { 'btn-icon-yellow': swap === fid }) }
            onClick={ () => swapFrames() }
          >
            <MdSwapHoriz title="Swap with another block" />
          </button>

          <button
            type="button"
            className="btn-icon"
            onClick={ () => openDevTools() }
          >
            <RiLayoutRightFill title="Open dev tools" />
          </button>

          <button
            type="button"
            className="btn-icon btn-icon-red"
            onClick={ () => deleteFrame() }
          >
            <TiDelete title="Remove the block" />
          </button>
        </div>
      </div>
    </div>
  );
}

export default Frame;
