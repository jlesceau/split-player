import cls from 'classnames';
import { map } from 'lodash';
import { useBranch } from 'baobab-react/hooks';
import React, { useState, useEffect } from 'react';

import actions from '../../core/actions';

import Frame from './Frame';

function Frames({ sideBarWidth }) {
  /**
   * Component State
   */
  const [dragging, setDragging] = useState({
    resizing: false,
    fid: null,
    x: 0,
    y: 0,
    newPos: null,
    newGridPos: null,
  });


  /**
   * Baobab State
   */
  const { swap, config, frames } = useBranch({
    swap: ['swap'],
    config: ['config'],
    frames: ['frames'],
  });


  /**
   * Handlers
   */
  const doubleClick = (e, fid) => {
    if (
      e.target.classList.contains('frame-resize')
        || e.target.classList.contains('frame')
    ) {
      let side = null;
      const gridPosition = {
        ...frames[fid].gridPosition,
      };

      e.stopPropagation();

      if (e.target.classList.contains('frame-resize')) {
        [, side] = e.target.classList;
      }

      switch (side) {
        case 'resize-top':
          gridPosition.top = 0;
          break;
        case 'resize-left':
          gridPosition.left = 0;
          break;
        case 'resize-bottom':
          gridPosition.bottom = config.grid.height - 1;
          break;
        case 'resize-right':
          gridPosition.right = config.grid.width - 1;
          break;
        case 'resize-tl':
          gridPosition.top = 0;
          gridPosition.left = 0;
          break;
        case 'resize-bl':
          gridPosition.bottom = config.grid.height - 1;
          gridPosition.left = 0;
          break;
        case 'resize-br':
          gridPosition.bottom = config.grid.height - 1;
          gridPosition.right = config.grid.width - 1;
          break;
        case 'resize-tr':
          gridPosition.top = 0;
          gridPosition.right = config.grid.width - 1;
          break;
        default:
          gridPosition.top = 0;
          gridPosition.left = 0;
          gridPosition.bottom = config.grid.height - 1;
          gridPosition.right = config.grid.width - 1;
      }

      actions.moveFrame({ fid, gridPosition });
    }
  };
  const down = (e, fid) => {
    const touch = e.changedTouches ? e.changedTouches[0] : null;

    if (
      !dragging.fid
      && (
        e.target.classList.contains('frame-resize')
        || e.target.classList.contains('frame')
      ) && (!touch || touch.identifier === 0)
    ) {
      e.stopPropagation();

      const resizing = e.target.classList.contains('frame-resize') ?
        e.target.classList[1] :
        false;

      setDragging({
        fid,
        resizing,
        x: touch ? touch.pageX : e.pageX,
        y: touch ? touch.pageY : e.pageY,
        newPos: {
          ...frames[fid].position,
        },
        newGridPos: {
          ...frames[fid].gridPosition,
        },
      });
    }
  };
  const move = e => {
    const frame = frames[dragging.fid];
    const { innerWidth, innerHeight } = window;
    const touch = e.changedTouches ? e.changedTouches[0] : null;

    if (
      frame
      && (!touch || touch.identifier === 0)
    ) {
      const x = touch ? touch.pageX : e.pageX;
      const y = touch ? touch.pageY : e.pageY;
      const widthUnit = (innerWidth - sideBarWidth) / config.grid.width;
      const heightUnit = innerHeight / config.grid.height;
      const offsetX = Math.round((x - dragging.x) / widthUnit);
      const offsetY = Math.round((y - dragging.y) / heightUnit);
      let newGridPos = {
        top: frame.gridPosition.top,
        left: frame.gridPosition.left,
        bottom: frame.gridPosition.bottom,
        right: frame.gridPosition.right,
      };

      switch (dragging.resizing) {
        case 'resize-top':
          newGridPos.top = frame.gridPosition.top + offsetY;
          break;
        case 'resize-left':
          newGridPos.left = frame.gridPosition.left + offsetX;
          break;
        case 'resize-bottom':
          newGridPos.bottom = frame.gridPosition.bottom + offsetY;
          break;
        case 'resize-right':
          newGridPos.right = frame.gridPosition.right + offsetX;
          break;
        case 'resize-tl':
          newGridPos.top = frame.gridPosition.top + offsetY;
          newGridPos.left = frame.gridPosition.left + offsetX;
          break;
        case 'resize-bl':
          newGridPos.bottom = frame.gridPosition.bottom + offsetY;
          newGridPos.left = frame.gridPosition.left + offsetX;
          break;
        case 'resize-br':
          newGridPos.bottom = frame.gridPosition.bottom + offsetY;
          newGridPos.right = frame.gridPosition.right + offsetX;
          break;
        case 'resize-tr':
          newGridPos.top = frame.gridPosition.top + offsetY;
          newGridPos.right = frame.gridPosition.right + offsetX;
          break;
        default: {
          const height = frame.gridPosition.bottom - frame.gridPosition.top;
          const width = frame.gridPosition.right - frame.gridPosition.left;

          newGridPos = {
            top: Math.max(0, frame.gridPosition.top + offsetY),
            left: Math.max(0, frame.gridPosition.left + offsetX),
          };
          newGridPos.bottom = newGridPos.top + height;
          newGridPos.right = newGridPos.left + width;

          if (newGridPos.bottom >= config.grid.height) {
            newGridPos.top = config.grid.height - 1 - height;
            newGridPos.bottom = config.grid.height - 1;
          }

          if (newGridPos.right >= config.grid.width) {
            newGridPos.left = config.grid.width - 1 - width;
            newGridPos.right = config.grid.width - 1;
          }
        }
      }

      if (
        newGridPos.top >= 0
        && newGridPos.left >= 0
        && newGridPos.bottom < config.grid.height
        && newGridPos.right < config.grid.width
      ) {
        setDragging({
          resizing: dragging.resizing,
          fid: dragging.fid,
          x: dragging.x,
          y: dragging.y,
          newGridPos,
          newPos: {
            top: Math.floor(newGridPos.top * heightUnit),
            left: sideBarWidth + Math.floor(newGridPos.left * widthUnit),
            width: Math.ceil(
              (newGridPos.right - newGridPos.left + 1) * widthUnit - 1
            ),
            height: Math.ceil(
              (newGridPos.bottom - newGridPos.top + 1) * heightUnit - 1
            ),
          },
        });
      }
    }
  };
  const up = e => {
    const frame = frames[dragging.fid];
    const touch = e.changedTouches ? e.changedTouches[0] : null;

    if (
      frame
      && (!touch || touch.identifier === 0)
    ) {
      actions.moveFrame({
        fid: frame.fid,
        gridPosition: dragging.newGridPos,
      });

      setDragging({
        resizing: false,
        id: null,
        x: 0,
        y: 0,
        newPos: null,
        newGridPos: null,
      });
    }
  };


  /**
   * Lifecycle
   */
  useEffect(() => {
    window.addEventListener('mousemove', move, false);
    window.addEventListener('mouseup', up, false);
    window.addEventListener('touchmove', move, false);
    window.addEventListener('touchend', up, false);
    window.addEventListener('touchcancel', up, false);

    return () => {
      window.removeEventListener('mousemove', move, false);
      window.removeEventListener('mouseup', up, false);
      window.removeEventListener('touchmove', move, false);
      window.removeEventListener('touchend', up, false);
      window.removeEventListener('touchcancel', up, false);
    };
  });


  /**
   * Render
   */
  return (
    <div className="frames">{
      map(frames, frame => {
        const position = dragging.fid === frame.fid ?
          dragging.newPos :
          frame.position;

        return (
          <div
            key={ frame.fid }
            className={
              cls('frame-wrapper', { dragging: dragging.fid === frame.fid })
            }
            style={{
              top: `${ position.top }px`,
              left: `${ position.left }px`,
              width: `${ position.width }px`,
              height: `${ position.height }px`,
            }}
            onMouseDown={ e => down(e, frame.fid) }
            onTouchStart={ e => down(e, frame.fid) }
            onDoubleClick={ e => doubleClick(e, frame.fid) }
          >
            <Frame
              swap={ swap }
              frame={ frame }
            />
            <div className="frame-resize resize-top" />
            <div className="frame-resize resize-left" />
            <div className="frame-resize resize-bottom" />
            <div className="frame-resize resize-right" />
            <div className="frame-resize resize-tl" />
            <div className="frame-resize resize-bl" />
            <div className="frame-resize resize-br" />
            <div className="frame-resize resize-tr" />
          </div>
        );
      })
    }</div>
  );
}

export default Frames;
