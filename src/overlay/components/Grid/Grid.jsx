import React from 'react';

function Grid({ sideBarWidth }) {
  return (
    <div
      className="grid"
      style={{
        left: `${ sideBarWidth }px`,
      }}
    >
      <div className="vertical multiple-2">
        <div className="grid-separator border-bottom" />
        <div className="grid-separator border-bottom" />
      </div>
      <div className="vertical multiple-3">
        <div className="grid-separator border-bottom" />
        <div className="grid-separator border-bottom" />
        <div className="grid-separator border-bottom" />
      </div>
      <div className="horizontal multiple-3">
        <div className="grid-separator border-right" />
        <div className="grid-separator border-right" />
        <div className="grid-separator border-right" />
      </div>
      <div className="horizontal multiple-2">
        <div className="grid-separator border-right" />
        <div className="grid-separator border-right" />
        <div className="grid-separator border-right" />
        <div className="grid-separator border-right" />
      </div>
      <div className="horizontal multiple-5">
        <div className="grid-separator border-right" />
        <div className="grid-separator border-right" />
        <div className="grid-separator border-right" />
        <div className="grid-separator border-right" />
        <div className="grid-separator border-right" />
      </div>
    </div>
  );
}

export default Grid;
