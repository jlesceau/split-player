import tree from './tree';
import socket from './socket';

const actions = {
  /**
   * Toggles
   */
  toggleGrid: () => {
    const { wid } = tree.get();

    socket.send('toggleGrid', { wid });
  },

  openGrid: () => {
    const { wid, displayGrid } = tree.get();

    if (!displayGrid) {
      socket.send('toggleGrid', { wid });
    }
  },

  closeGrid: () => {
    const { wid, displayGrid } = tree.get();

    if (displayGrid) {
      socket.send('toggleGrid', { wid });
    }
  },

  toggleSettings: () => {
    const { wid } = tree.get();

    socket.send('toggleSettings', { wid });
    tree.set(['request', 'categories'], []);
    tree.set(['request', 'channels'], []);
  },

  openSettings: () => {
    const { wid, displaySettings } = tree.get();

    if (!displaySettings) {
      socket.send('toggleSettings', { wid });
    }
  },

  closeSettings: () => {
    const { wid, displaySettings } = tree.get();

    if (displaySettings) {
      socket.send('toggleSettings', { wid });
      tree.set(['request', 'categories'], []);
      tree.set(['request', 'channels'], []);
    }
  },

  toggleFavorites: () => {
    const { wid } = tree.get();

    socket.send('toggleFavorites', { wid });
  },

  openFavorites: ({ fid } = {}) => {
    const { wid, displayFavorites } = tree.get();

    if (!displayFavorites) {
      socket.send('toggleFavorites', { wid, fid });
    }
  },

  closeFavorites: () => {
    const { wid, displayFavorites } = tree.get();

    if (displayFavorites) {
      socket.send('toggleFavorites', { wid });
    }
  },

  openSelector: ({ url, channel } = {}) => {
    const { wid } = tree.get();

    socket.send('toggleSelector', {
      wid,
      url: channel ?
        `https://www.twitch.tv/${ channel }` :
        url,
    });
  },

  closeSelector: () => {
    const { wid, displaySelector } = tree.get();

    if (displaySelector) {
      socket.send('toggleSelector', { wid });
    }
  },


  /**
   * Frame actions
   */
  addFrame: params => {
    const {
      url,
      channel,
      newWindow = false,
      row = 0,
      col = 0,
      rowCount = 1,
      colCount = 1,
    } = params;
    const { wid } = tree.get();

    socket.send('frame-add', {
      wid,
      newWindow,
      row,
      col,
      rowCount,
      colCount,
      url: channel ?
        `https://www.twitch.tv/${ channel }` :
        url,
    });
  },

  updateFrame: ({ fid, url, channel }) => {
    const { wid } = tree.get();

    socket.send('frame-updateChannel', {
      wid,
      fid,
      url: channel ?
        `https://www.twitch.tv/${ channel }` :
        url,
    });
  },

  reloadFrame: ({ fid }) => {
    const { wid } = tree.get();

    socket.send('frame-reload', {
      wid,
      fid,
    });
  },

  moveFrame: ({ fid, gridPosition }) => {
    const { wid } = tree.get();

    socket.send('frame-move', {
      wid,
      fid,
      gridPosition,
    });
  },

  swapFrames: ({ fid }) => {
    const { wid, swap } = tree.get();

    if (!swap) {
      tree.set('swap', fid);
    } else {
      if (swap !== fid) {
        socket.send('frame-swap', {
          wid,
          fid1: swap,
          fid2: fid,
        });
      }

      tree.set('swap', null);
    }
  },

  deleteFrame: ({ fid }) => {
    const { wid } = tree.get();

    socket.send('frame-remove', {
      wid,
      fid,
    });
  },

  openDevTools: ({ fid }) => {
    const { wid } = tree.get();

    socket.send('devtools', {
      wid,
      fid,
    });
  },


  /**
   * Tops
   */
  refreshTopsLive: () => {
    const { wid, request, loadings } = tree.get();

    if (!loadings.tops && (Date.now() - request.lastTops) > (2 * 60 * 1000)) {
      actions.clearTops();
      socket.send('twitch-getTopsLive', { wid });
    }
  },

  clearTops: () => {
    tree.set(['request', 'tops'], []);
  },


  /**
   * Settings
   */
  editConfig: ({ key, value }) => {
    socket.send('config-edit', {
      key,
      value,
    });
  },

  requestCategory: ({ query }) => {
    const { wid } = tree.get();

    socket.send('twitch-searchCategories', { wid, query });
  },

  requestChannel: ({ query }) => {
    const { wid } = tree.get();

    socket.send('twitch-searchChannels', { wid, query });
  },

  clearRequests: () => {
    tree.set(['request', 'channels'], []);
    tree.set(['request', 'categories'], []);
  },
};

export default actions;
