import { ipcRenderer } from 'electron';

import tree from './tree';

ipcRenderer.on('tree-update', (e, updates) => {
  updates.forEach(({ path, data }) => {
    tree.set(path, data);
  });
});

export default ipcRenderer;
