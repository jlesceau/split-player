import Baobab from 'baobab';

const tree = new Baobab({
  wid: null,
  displaySidebar: true,
  displayGrid: false,
  displaySettings: false,
  displayFavorites: false,
  displaySelector: false,
  config: {},
  frames: {},
  favorites: [],
  swap: null,
  request: {
    lastTops: 0,
    tops: [],
    channels: [],
    categories: [],
  },
  loadings: {
    tops: false,
  },
});

export default tree;
