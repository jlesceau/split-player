import { round } from 'lodash';

const rounders = [
  { max: 1000, divider: 1, unit: '' },
  { max: 1000000, divider: 1000, unit: ' k' },
  { max: Infinity, divider: 1000000, unit: ' M' },
];

function formatViewers(viewers) {
  if (!Number.isInteger(viewers)) {
    return '';
  }

  let i = 0;

  while (viewers > rounders[i].max) i++;

  const { divider, unit } = rounders[i];
  let str = `${ round(viewers / divider, 1) }`;

  if (divider > 1 && !str.includes('.')) {
    str += '.0';
  }

  str += unit;

  return str;
}

export { formatViewers };
