import { app } from 'electron';
import { resolve } from 'path';
import { promises as fs, readdirSync, rmSync, constants } from 'fs';

import Windows from './core/windows';

const tmpDir = resolve(app.getPath('temp'), 'split-player');

async function start() {
  // Create a tmp dir for the app
  try {
    await fs.access(tmpDir, constants.W_OK);
  } catch (ex) {
    await fs.mkdir(tmpDir);
  }

  const windows = new Windows();
  const restored = await windows.initLocalData();

  // Open the first window if nothing has been restored from a crash
  if (!restored) {
    windows.openWindow();
  }

  // Open new windows on new app launches
  app.on('second-instance', () => {
    windows.openWindow();
  });
}

function close() {
  // Empty tmp dir
  const files = readdirSync(tmpDir);

  files.forEach(fileName => {
    try {
      rmSync(resolve(tmpDir, fileName));
    } catch (ex) {} // eslint-disable-line
  });
}

app.commandLine.appendSwitch('disable-features', 'HardwareMediaKeyHandling');

// Modify the display name on Windows
if (process.platform === 'win32') {
  app.setAppUserModelId('Split Player');
}

// Make sure it's the first and only instance or quit
if (!app.requestSingleInstanceLock()) {
  app.quit();
}

app.on('ready', start);
app.on('will-quit', close);
