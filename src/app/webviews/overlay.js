import { BrowserView } from 'electron';

export default function loadView() {
  const view = new BrowserView({
    webPreferences: {
      transparent: true,
      contextIsolation: false,
      nodeIntegration: true,
    },
  });

  view.webContents.loadFile(`${ __dirname }/../../overlay/main.html`);

  return view;
}
