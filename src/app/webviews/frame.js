import { URL } from 'url';
import { join } from 'path';
import { BrowserView, shell } from 'electron';

export default function loadView({ wid, fid, url, scripts, updateURL }) {
  const view = new BrowserView({
    webPreferences: {
      disableHtmlFullscreenWindowResize: true,
      preload: join(__dirname, 'preload.js'),
      additionalArguments: [
        `__twitchTweaksWID=${ wid }`,
        `__twitchTweaksFID=${ fid }`,
      ],
    },
  });
  let prevUrl = url;

  view.webContents.on('dom-ready', () => {
    const domain = new URL(view.webContents.getURL()).hostname;

    if (scripts[domain]) {
      if (scripts[domain].js) {
        view.webContents.executeJavaScript(scripts[domain].js);
      }

      if (scripts[domain].css) {
        view.webContents.insertCSS(scripts[domain].css);
      }
    }
  });

  view.webContents.on('did-start-navigation', (e, newUrl, p, isMainFrame) => {
    if (isMainFrame && prevUrl !== newUrl) {
      updateURL(newUrl);

      prevUrl = newUrl;
    }
  });

  view.webContents.setWindowOpenHandler(details => {
    shell.openExternal(details.url);

    return { action: 'deny' };
  });

  view.webContents.loadURL(
    url,
    {
      userAgent: [
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64)',
        'AppleWebKit/537.36 (KHTML, like Gecko)',
        `Chrome/${ process.versions.chrome }`,
        'Safari/537.36',
      ].join(' '),
    }
  );

  return view;
}
