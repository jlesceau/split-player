import { contextBridge, ipcRenderer } from 'electron';

if (window.location.hostname === 'www.twitch.tv') {
  let wid;
  let fid;

  process.argv.forEach(arg => {
    if (arg.startsWith('__twitchTweaksWID=')) {
      [, wid] = arg.split('=');
    } else if (arg.startsWith('__twitchTweaksFID=')) {
      [, fid] = arg.split('=');
    }
  });

  contextBridge.exposeInMainWorld('__twitchTweaksAPI', {
    onVIPs: cb => ipcRenderer.on('VIPs', (e, data) => cb(data)),
    onClip: cb => ipcRenderer.on('clip', (e, data) => cb(data)),
    onUser: cb => ipcRenderer.on('user', (e, data) => cb(data)),
    onBadges: cb => ipcRenderer.on('badges', (e, data) => cb(data)),
    onEmotes: cb => ipcRenderer.on('emotes', (e, data) => cb(data)),

    getVIPs: body => ipcRenderer.send('getVIPs', { ...body, wid, fid }),
    getClip: body => ipcRenderer.send('getClip', { ...body, wid, fid }),
    getUser: body => ipcRenderer.send('getUser', { ...body, wid, fid }),
    getBadges: body => ipcRenderer.send('getBadges', { ...body, wid, fid }),
    getEmotes: body => ipcRenderer.send('getEmotes', { ...body, wid, fid }),
  });
}
