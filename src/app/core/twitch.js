import { chunk } from 'lodash';

const urls = {
  bttv: {
    global: 'https://api.betterttv.net/3/cached/emotes/global',
    channel: id => `https://api.betterttv.net/3/cached/users/twitch/${ id }`,
  },
  '7tv': {
    global: 'https://7tv.io/v3/emote-sets/global',
    channel: id => `https://7tv.io/v3/users/twitch/${ id }`,
  },
  ffz: {
    channel: id => `https://api.frankerfacez.com/v1/room/id/${ id }`,
  },
};

class TwitchClient {
  constructor() {
    this.credentials = null;
  }

  async setCredentials(credentials) {
    if (
      !credentials.authUrl
        || !credentials.url
        || !credentials.appId
        || !credentials.appSecret
    ) {
      this.credentials = null;

      return null;
    }

    this.credentials = {
      ...credentials,
    };

    // Check if token exists & doesn't expire in less than 2 days
    if (this.credentials.token) {
      const body = await (
        await fetch(
          `${ this.credentials.authUrl }/validate`,
          {
            method: 'GET',
            headers: {
              Authorization: `OAuth ${ this.credentials.token }`,
            },
          }
        )
      ).json();

      if (body.status === 401 || body.expires_in < (2 * 24 * 60 * 60)) {
        this.credentials.token = null;
      }
    }

    // Get a token if necessary
    if (!this.credentials.token) {
      const q = [
        `client_id=${ this.credentials.appId }`,
        `client_secret=${ this.credentials.appSecret }`,
        'grant_type=client_credentials',
      ].join('&');
      const body = await (
        await fetch(
          `${ this.credentials.authUrl }/token?${ q }`,
          { method: 'POST' }
        )
      ).json();

      if (body.access_token) {
        this.credentials.token = body.access_token;
      }
    }

    if (!this.credentials.token) {
      this.credentials = null;

      return null;
    }

    return {
      ...this.credentials,
    };
  }

  async getChannelsBasicInfos(ids) {
    if (!this.credentials || !ids.length) return {};

    const cache = {};
    const chunks = chunk(ids, 100);
    const calls = chunks.map(async idsChunk => {
      const q = [
        `id=${ idsChunk.join('&id=') }`,
      ].join('&');
      const response = await fetch(
        `${ this.credentials.url }/users?${ q }`,
        {
          method: 'GET',
          headers: {
            'client-id': this.credentials.appId,
            Authorization: `Bearer ${ this.credentials.token }`,
          },
        }
      );

      return response.json();
    });
    const fetchedData = (await Promise.all(calls))
      .reduce((res, body) => [...res, ...(body.data || [])], []);

    fetchedData.forEach(channel => {
      cache[channel.id] = {
        id: channel.id,
        channel: channel.login,
        name: channel.display_name,
        avatar: channel.profile_image_url,
        broadcasterType: channel.broadcaster_type,
        description: channel.description,
        createdAt: new Date(channel.created_at).getTime(),
        lastRefresh: Date.now(),
      };
    });

    return cache;
  }

  async getLiveChannelsInfos(ids) {
    if (!this.credentials || !ids.length) return {};

    const data = {};
    const chunks = chunk(ids, 100);
    const calls = chunks.map(async idsChunk => {
      const q = [
        `user_id=${ idsChunk.join('&user_id=') }`,
        'first=100',
      ].join('&');
      const response = await fetch(
        `${ this.credentials.url }/streams?${ q }`,
        {
          method: 'GET',
          headers: {
            'client-id': this.credentials.appId,
            Authorization: `Bearer ${ this.credentials.token }`,
          },
        }
      );

      return response.json();
    });
    const fetchedData = (await Promise.all(calls))
      .reduce((res, body) => [...res, ...(body.data || [])], []);

    fetchedData.forEach(channel => {
      data[channel.user_id] = {
        title: channel.title,
        game: channel.game_name,
        lang: channel.language,
        viewers: channel.viewer_count,
        startedAt: channel.started_at,
      };
    });

    return data;
  }

  async getTopChannelsInfos(category, top) {
    if (!this.credentials) return [];

    const q = [
      category.id ? `game_id=${ category.id }` : '',
      `first=${ top }`,
      category.lang ? `language=${ category.lang }` : '',
    ].filter(v => !!v).join('&');
    const response = await fetch(
      `${ this.credentials.url }/streams?${ q }`,
      {
        method: 'GET',
        headers: {
          'client-id': this.credentials.appId,
          Authorization: `Bearer ${ this.credentials.token }`,
        },
      }
    );
    const body = await response.json();

    return (body.data || []).map(channel => ({
      id: channel.user_id,
      type: 'twitch',
      name: channel.user_name,
      channel: channel.user_name,
      avatar: null,
      isLive: true,
      title: channel.title,
      game: channel.game_name,
      lang: channel.language,
      viewers: channel.viewer_count,
      startedAt: channel.started_at,
    }));
  }

  async searchChannels(query) {
    if (!this.credentials) return [];

    const q = [
      `query=${ query }`,
      'first=20',
    ].join('&');
    const body = await (
      await fetch(
        `${ this.credentials.url }/search/channels?${ q }`,
        {
          method: 'GET',
          headers: {
            'client-id': this.credentials.appId,
            Authorization: `Bearer ${ this.credentials.token }`,
          },
        }
      )
    ).json();

    return Array.isArray(body.data) ? body.data : [];
  }

  async searchCategories(query) {
    if (!this.credentials) return [];

    const q = [
      `query=${ query }`,
      'first=20',
    ].join('&');
    const body = await (
      await fetch(
        `${ this.credentials.url }/search/categories?${ q }`,
        {
          method: 'GET',
          headers: {
            'client-id': this.credentials.appId,
            Authorization: `Bearer ${ this.credentials.token }`,
          },
        }
      )
    ).json();

    return Array.isArray(body.data) ? body.data : [];
  }

  async getCategory({ id }) {
    if (!this.credentials) return null;

    const q = [
      `id=${ id }`,
    ].join('&');
    const body = await (
      await fetch(
        `${ this.credentials.url }/games?${ q }`,
        {
          method: 'GET',
          headers: {
            'client-id': this.credentials.appId,
            Authorization: `Bearer ${ this.credentials.token }`,
          },
        }
      )
    ).json();

    return body?.data?.[0];
  }

  async getClip({ clipId }) {
    if (!this.credentials) return null;

    const q = [
      `id=${ clipId }`,
    ].join('&');
    const body = await (
      await fetch(
        `${ this.credentials.url }/clips?${ q }`,
        {
          method: 'GET',
          headers: {
            'client-id': this.credentials.appId,
            Authorization: `Bearer ${ this.credentials.token }`,
          },
        }
      )
    ).json();
    const clip = body?.data?.[0];
    let game;

    if (clip?.game_id) {
      game = await this.getCategory({ id: clip?.game_id });
    }

    return clip ?
      {
        id: clipId,
        title: clip.title,
        streamer: clip.broadcaster_name,
        creator: clip.creator_name,
        thumbnail: clip.thumbnail_url,
        url: clip.url,
        game: game?.name,
      } :
      null;
  }

  async getBadges({ channelId } = {}) {
    if (!this.credentials) return [];

    const q = channelId ?
      `?broadcaster_id=${ channelId }` :
      '/global';
    const body = await (
      await fetch(
        `${ this.credentials.url }/chat/badges${ q }`,
        {
          method: 'GET',
          headers: {
            'client-id': this.credentials.appId,
            Authorization: `Bearer ${ this.credentials.token }`,
          },
        }
      )
    ).json();

    return body?.data?.reduce((list, { set_id, versions }) => ([
      ...list,
      ...versions.map(({ id, image_url_2x }) => ([
        `${ set_id }/${ id }`,
        image_url_2x,
      ])),
    ]), []) || [];
  }

  async getEmotes({ channelId } = {}) {
    let emotes;

    if (!channelId) {
      emotes = {
        bttv: await this.getBTTVGlobalEmotes(),
        '7tv': await this.get7TVGlobalEmotes(),
      };
    } else {
      emotes = {
        bttv: await this.getBTTVEmotes({ channelId }),
        '7tv': await this.get7TVEmotes({ channelId }),
        ffz: await this.getFFZEmotes({ channelId }),
      };
    }

    return emotes;
  }

  async getBTTVGlobalEmotes() {
    const req = await fetch(urls.bttv.global);

    if (!req.ok) return [];

    const emotes = await req.json();

    return emotes?.map(em => ([
      em.code,
      { id: em.id },
    ])) || [];
  }

  async get7TVGlobalEmotes() {
    const req = await fetch(urls['7tv'].global);

    if (!req.ok) return [];

    const emotesSet = await req.json();

    return emotesSet?.emotes?.map(em => ([
      em.name,
      { id: em.id, by: em.data?.owner?.display_name },
    ])) || [];
  }

  async getBTTVEmotes({ channelId }) {
    const req = await fetch(urls.bttv.channel(channelId));

    if (!req.ok) return [];

    const emotesSet = await req.json();

    return emotesSet?.channelEmotes
      ?.concat(emotesSet?.sharedEmotes)
      .map(em => ([
        em.code,
        { id: em.id, by: em.user?.displayName },
      ])) || [];
  }

  async get7TVEmotes({ channelId }) {
    const req = await fetch(urls['7tv'].channel(channelId));

    if (!req.ok) return [];

    const emotesSet = await req.json();

    return emotesSet?.emote_set?.emotes?.map(em => ([
      em.name,
      { id: em.id, by: em.data?.owner?.display_name },
    ])) || [];
  }

  async getFFZEmotes({ channelId }) {
    const req = await fetch(urls.ffz.channel(channelId));

    if (!req.ok) return [];

    const emotesSet = await req.json();

    return Object.values(emotesSet?.sets || {})
      .map(set => set.emoticons)
      .flat()
      .map(em => ([
        em.name,
        { id: em.id, by: em.owner?.display_name },
      ]));
  }
}

export default TwitchClient;
