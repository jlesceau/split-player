import { resolve } from 'path';
import { promisify } from 'util';
import { pipeline } from 'stream';
import { app, Notification } from 'electron';
import { promises as fs, createWriteStream, constants } from 'fs';

class Notifications {
  constructor(context) {
    this.context = context;

    this.tmpDir = resolve(app.getPath('temp'), 'split-player');
  }

  async fetchImg({ type, favName, imgURL, imgPath }) {
    const streamPipeline = promisify(pipeline);
    let img = `${ __dirname }/../icon.ico`;

    if (imgURL) {
      const imgTmpPath = resolve(
        this.tmpDir,
        `notif-img-${ type }-${ favName }.png`
      );

      try {
        await fs.access(imgTmpPath, constants.R_OK);

        img = imgTmpPath;
      } catch (ex) {
        const res = await fetch(imgURL);

        if (res.ok) {
          await streamPipeline(res.body, createWriteStream(imgTmpPath));

          img = imgTmpPath;
        }
      }
    } else if (imgPath) {
      img = imgPath;
    }

    return img;
  }

  showNotif({ channel, title, body, icon }) {
    const notif = new Notification({
      title,
      body,
      icon,
      timeoutType: 'default',
    });

    notif.on('click', () => this.context.openNotif({
      url: `https://www.twitch.tv/${ channel }`,
    }));

    notif.show();
  }

  async showNotifs(notifs) {
    const calls = notifs.map(async notif => {
      const icon = await this.fetchImg(notif);

      this.showNotif({
        ...notif,
        icon,
      });
    });

    Promise.all(calls);
  }
}

export default Notifications;
