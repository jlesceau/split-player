import { app, ipcMain } from 'electron';
import { map, omit, forEach } from 'lodash';

import LocalData from './localData';
import Notifications from './notifications';
import PlayerWindow from '../windows/player';

class Windows {
  constructor() {
    this.windows = {};
    this.localData = new LocalData({
      updateConfig: this.updateConfig.bind(this),
      updateFavorites: this.updateFavorites.bind(this),
      showNotifs: this.showNotifs.bind(this),
    });
    this.notifications = new Notifications({
      openNotif: this.openNotif.bind(this),
    });

    // Overlay events
    ipcMain.on('frame-add', this.createFrame.bind(this));
    ipcMain.on('frame-move', this.moveFrame.bind(this));
    ipcMain.on('frame-updateChannel', this.updateFrame.bind(this));
    ipcMain.on('frame-reload', this.reloadFrame.bind(this));
    ipcMain.on('frame-swap', this.swapFrames.bind(this));
    ipcMain.on('frame-remove', this.removeFrame.bind(this));
    ipcMain.on('toggleGrid', this.toggleGrid.bind(this));
    ipcMain.on('toggleSettings', this.toggleSettings.bind(this));
    ipcMain.on('toggleFavorites', this.toggleFavorites.bind(this));
    ipcMain.on('toggleSelector', this.toggleSelector.bind(this));
    ipcMain.on('devtools', this.toggleDevtools.bind(this));
    ipcMain.on('config-edit', this.editConfig.bind(this));
    ipcMain.on('twitch-searchChannels', this.twitchSearchChannels.bind(this));
    ipcMain.on(
      'twitch-searchCategories',
      this.twitchSearchCategories.bind(this)
    );
    ipcMain.on('twitch-getTopsLive', this.twitchGetTopsLive.bind(this));

    // Frame events
    ipcMain.on('getVIPs', this.twitchGetVIPs.bind(this));
    ipcMain.on('getClip', this.twitchGetClip.bind(this));
    ipcMain.on('getUser', this.twitchGetUser.bind(this));
    ipcMain.on('getBadges', this.twitchGetBadges.bind(this));
    ipcMain.on('getEmotes', this.twitchGetEmotes.bind(this));
  }

  restart() {
    app.relaunch({ execPath: process.env.PORTABLE_EXECUTABLE_FILE });
    app.exit(0);
  }

  saveExit() {
    app.exit(0);
  }



  /**
   * LOCALDATA
   */
  async initLocalData() {
    await this.localData.init();

    if (app.isPackaged) {
      const prevLayout = this.localData.getCache().layout;

      if (Object.keys(prevLayout).length) {
        this.localData.saveCache('layout', {});

        forEach(prevLayout, layout => {
          this.openWindow(layout);
        });

        return true;
      }
    }

    return false;
  }

  getConfig() {
    return this.localData.getConfig();
  }

  getFavorites() {
    return this.localData.getFavorites();
  }

  getScripts() {
    return this.localData.getScripts();
  }

  editConfig(e, data) {
    this.localData.saveConfig(data);
  }



  /**
   * TWITCH PROXY
   */
  async twitchSearchChannels(e, data) {
    const channels = await this.localData.twitch.searchChannels(data.query);

    this.windows[data.wid].sendTreeUpdate({
      path: ['request', 'channels'],
      data: channels,
    });
  }

  async twitchSearchCategories(e, data) {
    const categories = await this.localData.twitch.searchCategories(data.query);

    this.windows[data.wid].sendTreeUpdate({
      path: ['request', 'categories'],
      data: categories,
    });
  }

  async twitchGetTopsLive(e, data) {
    this.windows[data.wid].sendTreeUpdate({
      path: ['loadings', 'tops'],
      data: true,
    });

    const tops = await this.localData.refreshTopsLive();

    this.windows[data.wid].sendTreeUpdate([
      { path: ['loadings', 'tops'], data: false },
      { path: ['request', 'tops'], data: tops },
      { path: ['request', 'lastTops'], data: Date.now() },
    ]);
  }

  twitchGetVIPs(e, data) {
    const VIPs = this.localData.getVIPs();

    this.windows[data.wid].sendFrameData({
      fid: data.fid,
      event: 'VIPs',
      data: { VIPs },
    });
  }

  async twitchGetClip(e, data) {
    const clip = await this.localData.getClip(data);

    this.windows[data.wid].sendFrameData({
      fid: data.fid,
      event: 'clip',
      data: { clip },
    });
  }

  async twitchGetUser(e, data) {
    const user = await this.localData.getUser(data);

    this.windows[data.wid].sendFrameData({
      fid: data.fid,
      event: 'user',
      data: { user },
    });
  }

  async twitchGetBadges(e, data) {
    const badges = await this.localData.getBadges(data);

    this.windows[data.wid].sendFrameData({
      fid: data.fid,
      event: 'badges',
      data: { badges },
    });
  }

  async twitchGetEmotes(e, data) {
    const emotes = await this.localData.getEmotes(data);

    this.windows[data.wid].sendFrameData({
      fid: data.fid,
      event: 'emotes',
      data: { emotes },
    });
  }



  /**
   * WINDOWS
   */
  openWindow(layout) {
    const win = new PlayerWindow({
      getConfig: this.getConfig.bind(this),
      getFavorites: this.getFavorites.bind(this),
      saveLayout: this.saveLayout.bind(this),
      openWindow: this.openWindow.bind(this),
      removeWindow: this.removeWindow.bind(this),
      restart: this.restart.bind(this),
      saveExit: this.saveExit.bind(this),
      getScripts: this.getScripts.bind(this),
      emptyMemoryCache: this.emptyMemoryCache.bind(this),
      emptyFullCache: this.emptyFullCache.bind(this),
    }, layout);

    this.windows[win.wid] = win;

    return win;
  }

  createFrame(e, data) {
    if (data.newWindow) {
      const win = this.openWindow();

      win.elements.overlay.webContents.on('did-finish-load', () => {
        win.createFrame(data);
      });
    } else if (data.wid && this.windows[data.wid]) {
      this.windows[data.wid].createFrame(data);
    }
  }

  moveFrame(e, data) {
    if (data.wid && this.windows[data.wid]) {
      this.windows[data.wid].moveFrame(data);
    }
  }

  updateFrame(e, data) {
    if (data.wid && this.windows[data.wid]) {
      this.windows[data.wid].updateFrame(data);
    }
  }

  reloadFrame(e, data) {
    if (data.wid && this.windows[data.wid]) {
      this.windows[data.wid].reloadFrame(data);
    }
  }

  swapFrames(e, data) {
    if (data.wid && this.windows[data.wid]) {
      this.windows[data.wid].swapFrames(data);
    }
  }

  removeFrame(e, data) {
    if (data.wid && this.windows[data.wid]) {
      this.windows[data.wid].removeFrame(data);
    }
  }

  toggleGrid(e, data) {
    if (data.wid && this.windows[data.wid]) {
      this.windows[data.wid].toggleGrid(data);
    }
  }

  toggleSettings(e, data) {
    if (data.wid && this.windows[data.wid]) {
      this.windows[data.wid].toggleSettings(data);
    }
  }

  toggleFavorites(e, data) {
    if (data.wid && this.windows[data.wid]) {
      this.windows[data.wid].toggleFavorites(data);
    }
  }

  toggleSelector(e, data) {
    if (data.wid && this.windows[data.wid]) {
      this.windows[data.wid].toggleSelector(data);
    }
  }

  toggleDevtools(e, data) {
    if (data.wid && this.windows[data.wid]) {
      this.windows[data.wid].toggleDevtools(data);
    }
  }

  updateConfig() {
    forEach(this.windows, win => win.sendConfig());
  }

  updateFavorites() {
    forEach(this.windows, win => win.sendFavorites());
  }

  saveLayout(wid, layout) {
    if (app.isPackaged) {
      this.localData.saveCache('layout', {
        ...this.localData.getCache().layout,
        [wid]: layout,
      });
    }
  }

  emptyMemoryCache() {
    this.localData.memoryCache = {};
  }

  emptyFullCache() {
    this.localData.memoryCache = {};
    this.localData.saveCache('favorites', {});
    this.localData.reloadFavorites();
  }

  removeWindow(wid) {
    this.windows = omit(this.windows, wid);
    this.localData.saveCache(
      'layout',
      omit(this.localData.getCache().layout, wid)
    );
  }



  /**
   * NOTIFS
   */
  showNotifs(notifs) {
    this.notifications.showNotifs(notifs);
  }

  openNotif(notif) {
    map(this.windows, win => {
      win.toggleSelector(notif);
    });
  }
}

export default Windows;
