import { app, session } from 'electron';
import { resolve, dirname } from 'path';
import { set, uniq, cloneDeep } from 'lodash';
import { promises as fs, constants } from 'fs';
import { ElectronBlocker } from '@cliqz/adblocker-electron';

import TwitchClient from './twitch';

class LocalData {
  constructor(context) {
    this.context = context;

    this.documentsPath = `${ app.getPath('documents') }/SplitPlayer`;
    this.appDataPath =
      `${ app.getPath('appData') }/split-player/Split Player`;
    this.extensionsPath = resolve(
      app.getPath('appData'),
      '..',
      'Local/Google/Chrome/User Data/Default/Extensions'
    );
    this.configPath = app.isPackaged ?
      `${ this.documentsPath }/config.json` :
      `${ this.documentsPath }/configDev.json`;
    this.cachePath = `${ this.appDataPath }/cache.json`;
    this.scriptsPath = `${ this.documentsPath }/scripts`;

    this.defaultConfigPath = `${ __dirname }/../localData/defaultConfig.json`;
    this.defaultCachePath = `${ __dirname }/../localData/defaultCache.json`;

    this.config = {};
    this.cache = {};
    this.memoryCache = {};
    this.scripts = {};
    this.favorites = [];
    this.VIPs = [];

    this.twitch = new TwitchClient();

    this.adblocker = null;

    this.intervalId = null;
  }

  async init() {
    await this.loadConfig();
    await this.loadCache();
    await this.setClientsCredentials();
    await this.reloadExtensions();
    await this.reloadAdblocker();
    await this.reloadScripts();
    await this.reloadFavorites();
  }



  /**
   * CONFIG
   */
  async loadConfig() {
    try {
      await fs.access(this.configPath, constants.W_OK);
    } catch (ex) {
      const defaultConfig = await fs.readFile(this.defaultConfigPath, 'utf8');

      await fs.mkdir(dirname(this.configPath), { recursive: true });
      await fs.writeFile(this.configPath, defaultConfig);
    }

    this.config = JSON.parse(await fs.readFile(this.configPath, 'utf8'));
  }

  getConfig() {
    return cloneDeep(this.config);
  }

  async saveConfig({ key, value, skipReloadCreds }) {
    set(this.config, key, cloneDeep(value));

    if (key.startsWith('credentials') && !skipReloadCreds) {
      await this.setClientsCredentials();
      await this.reloadFavorites();
    } else if (key === 'favorites') {
      await this.reloadFavorites();
    } else if (key === 'extensions') {
      await this.reloadExtensions();
    } else if (key === 'useAdblocker') {
      await this.reloadAdblocker();
    } else if (key === 'scripts') {
      await this.reloadScripts();
    }

    await fs.writeFile(
      this.configPath,
      JSON.stringify(this.config, null, '  ')
    );

    this.context.updateConfig();
  }

  async setClientsCredentials() {
    const creds = await this.twitch.setCredentials(this.config.credentials);

    if (creds) {
      await this.saveConfig({
        key: 'credentials',
        value: creds,
        skipReloadCreds: true,
      });
    }
  }



  /**
   * CACHE
   */
  async loadCache() {
    try {
      await fs.access(this.cachePath, constants.W_OK);
      JSON.parse(await fs.readFile(this.cachePath, 'utf8'));
    } catch (ex) {
      const defaultCache = await fs.readFile(this.defaultCachePath, 'utf8');

      await fs.mkdir(dirname(this.cachePath), { recursive: true });
      await fs.writeFile(this.cachePath, defaultCache);
    }

    this.cache = JSON.parse(await fs.readFile(this.cachePath, 'utf8'));
  }

  getCache() {
    return cloneDeep(this.cache);
  }

  async saveCache(key, cache) {
    this.cache = {
      ...this.cache,
      [key]: cloneDeep(cache),
    };

    await fs.writeFile(this.cachePath, JSON.stringify(this.cache, null, '  '));
  }



  /**
   * EXTENSIONS
   */
  async reloadExtensions() {
    // Unload previously loaded extensions
    session.defaultSession
      .getAllExtensions()
      .forEach(({ id }) => session.defaultSession.removeExtension(id));

    const calls = this.config.extensions.map(async ext => {
      try {
        const extPath = `${ this.extensionsPath }/${ ext }`;
        const extVersions = await fs.readdir(extPath);
        const lastVersion = extVersions.sort().pop();

        if (lastVersion) {
          await session.defaultSession.loadExtension(
            `${ extPath }/${ lastVersion }`
          );
        }
      } catch (ex) {} // eslint-disable-line
    });

    Promise.all(calls);
  }



  /**
   * ADBLOCKER
   */
  async reloadAdblocker() {
    try {
      if (!this.adblocker) {
        this.adblocker = await ElectronBlocker
          .fromPrebuiltAdsAndTracking(fetch);
      }

      if (
        this.adblocker
          && this.adblocker.isBlockingEnabled(session.defaultSession)
      ) {
        this.adblocker.disableBlockingInSession(session.defaultSession);
      }

      if (this.adblocker && this.config.useAdblocker) {
        this.adblocker.enableBlockingInSession(session.defaultSession);
      }
    } catch (ex) {} // eslint-disable-line
  }



  /**
   * SCRIPTS
   */
  async reloadScripts() {
    try {
      await fs.access(this.scriptsPath, constants.W_OK);
    } catch (ex) {
      await fs.mkdir(this.scriptsPath, { recursive: true });
    }

    const scripts = {};
    const calls = this.config.scripts.map(async script => {
      scripts[script.domain] = {
        js: null,
        css: null,
      };

      if (script.js) {
        try {
          scripts[script.domain].js = await fs.readFile(
            `${ this.scriptsPath }/${ script.js }`,
            'utf8'
          );
        } catch (ex) {} // eslint-disable-line
      }

      if (script.css) {
        try {
          scripts[script.domain].css = await fs.readFile(
            `${ this.scriptsPath }/${ script.css }`,
            'utf8'
          );
        } catch (ex) {} // eslint-disable-line
      }
    });

    await Promise.all(calls);

    this.scripts = scripts;
  }

  getScripts() {
    return this.scripts;
  }


  /**
   * FAVORITES
   */
  getFavorites() {
    return cloneDeep(this.favorites);
  }

  getVIPs() {
    return cloneDeep(this.VIPs);
  }

  reloadVIPs() {
    this.VIPs = [
      ...this.favorites.reduce((list, cat) => ([
        ...list,
        ...cat.channels.map(({ name }) => name),
      ]), []),
      ...(this.config.VIPs || []),
    ].map(name => name.toLowerCase());
  }

  async reloadFavorites() {
    clearInterval(this.intervalId);

    const cache = cloneDeep(this.cache.favorites);
    const ids = this.config.favorites
      .reduce((list, group) => [...list, ...group.channels], [])
      .filter(channel => (
        channel.type === 'twitch'
        && (
          !this.cache.favorites[channel.id]
          || (
            this.cache.favorites[channel.id].lastRefresh
              < (Date.now() - (this.config.cacheExpiration * 86400000))
          )
        )
      ))
      .map(channel => channel.id);
    const refreshedCache = await this.twitch.getChannelsBasicInfos(ids);

    this.saveCache('favorites', {
      ...cache,
      ...refreshedCache,
    });

    this.favorites = this.config.favorites.map(group => ({
      groupName: group.groupName,
      channels: group.channels.map(channel => {
        const data = {
          id: channel.id,
          type: channel.type,
          favName: channel.favName,
          notification: channel.notification,
          displayInSideBar: channel.displayInSideBar,
          isLive: false,
          title: '',
          game: '',
          viewers: null,
          startedAt: null,
        };

        if (!this.cache.favorites[channel.id]) {
          return {
            ...data,
            channel: '',
            name: '',
            avatar: '',
            lastRefresh: 0,
          };
        }

        return {
          ...data,
          ...this.cache.favorites[channel.id],
        };
      }),
    }));

    this.reloadVIPs();
    this.context.updateFavorites();
    this.refreshFavoritesLive({ activeNotifs: false });

    this.intervalId = setInterval(() => {
      this.refreshFavoritesLive({ activeNotifs: true });
    }, this.config.channelsRefreshRate * 60 * 1000);
  }

  async refreshFavoritesLive({ activeNotifs }) {
    const ids = this.config.favorites
      .reduce((list, group) => [...list, ...group.channels], [])
      .filter(channel => channel.type === 'twitch')
      .map(channel => channel.id);
    const data = await this.twitch.getLiveChannelsInfos(ids);
    const notifs = [];

    this.favorites = this.favorites.map(group => ({
      groupName: group.groupName,
      channels: group.channels.map(channel => {
        // When live, update live infos
        if (data[channel.id]) {
          if (activeNotifs && !channel.isLive && channel.notification) {
            notifs.push({
              type: channel.type,
              favName: channel.favName,
              channel: channel.channel,
              title: `${ channel.name }`,
              body: `${ data[channel.id].game }\n${ data[channel.id].title }`,
              imgURL: channel.avatar,
            });
          }

          return {
            ...channel,
            isLive: true,
            ...data[channel.id],
          };

        // When was live but not anymore, remove live infos
        } else if (channel.isLive) {
          return {
            ...channel,
            isLive: false,
            title: '',
            game: '',
            viewers: null,
            startedAt: null,
          };
        }

        return { ...channel };
      }),
    }));

    this.context.updateFavorites();
    this.context.showNotifs(notifs);
  }

  async refreshTopsLive() {
    const cache = cloneDeep(this.cache.favorites);
    const calls = this.config.tops.categories.map(async category => {
      const channels = await this.twitch.getTopChannelsInfos(
        { ...category },
        this.config.tops.top
      );

      return {
        ...category,
        channels,
      };
    });
    const categories = await Promise.all(calls);
    const ids = categories
      .reduce((list, category) => [...list, ...category.channels], [])
      .filter(channel => (
        !this.cache.favorites[channel.id]
        || (
          this.cache.favorites[channel.id].lastRefresh
            < (Date.now() - (this.config.cacheExpiration * 86400000))
        )
      ))
      .map(channel => channel.id);
    const refreshedCache = await this.twitch.getChannelsBasicInfos(uniq(ids));

    this.saveCache('favorites', {
      ...cache,
      ...refreshedCache,
    });

    return categories.map(category => ({
      ...category,
      channels: category.channels.map(channel => ({
        ...channel,
        ...this.cache.favorites[channel.id],
      })),
    }));
  }


  /**
   * TWITCH PROXY
   */
  async getClip({ clipId }) {
    if (!this.memoryCache.clips) {
      this.memoryCache.clips = {};
    }

    if (!this.memoryCache.clips[clipId]) {
      this.memoryCache.clips[clipId] = await this.twitch.getClip({ clipId });
    }

    return cloneDeep(this.memoryCache.clips[clipId]);
  }

  async getUser({ userId }) {
    let cache = {};

    if (
      !this.cache.favorites[userId]
      || (
        this.cache.favorites[userId].lastRefresh
          < (Date.now() - (this.config.cacheExpiration * 86400000))
      )
    ) {
      cache = await this.twitch.getChannelsBasicInfos([userId]);
    }

    this.saveCache('favorites', {
      ...this.cache.favorites,
      ...cache,
    });

    return cloneDeep(this.cache.favorites[userId]);
  }

  async getBadges({ channelId }) {
    if (!this.memoryCache.globalBadges) {
      this.memoryCache.globalBadges = await this.twitch.getBadges();
    }

    if (!this.memoryCache.channelsBadges) {
      this.memoryCache.channelsBadges = {};
    }

    if (!this.memoryCache.channelsBadges[channelId]) {
      this.memoryCache.channelsBadges[channelId] = await this.twitch
        .getBadges({ channelId });
    }

    return [
      ...cloneDeep(this.memoryCache.globalBadges),
      ...cloneDeep(this.memoryCache.channelsBadges[channelId]),
    ];
  }

  async getEmotes({ channelId }) {
    if (!this.memoryCache.globalEmotes) {
      this.memoryCache.globalEmotes = await this.twitch.getEmotes();
    }

    if (!this.memoryCache.channelsEmotes) {
      this.memoryCache.channelsEmotes = {};
    }

    if (!this.memoryCache.channelsEmotes[channelId]) {
      this.memoryCache.channelsEmotes[channelId] = await this.twitch
        .getEmotes({ channelId });
    }

    return {
      bttv: [
        ...cloneDeep(this.memoryCache.globalEmotes.bttv),
        ...cloneDeep(this.memoryCache.channelsEmotes[channelId].bttv),
      ],
      '7tv': [
        ...cloneDeep(this.memoryCache.globalEmotes['7tv']),
        ...cloneDeep(this.memoryCache.channelsEmotes[channelId]['7tv']),
      ],
      ffz: [
        ...cloneDeep(this.memoryCache.channelsEmotes[channelId].ffz),
      ],
    };
  }
}

export default LocalData;
