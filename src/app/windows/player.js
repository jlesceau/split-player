import { v4 } from 'uuid';
import { forEach, map, omit } from 'lodash';
import { BrowserWindow, Menu } from 'electron';

import loadFrame from '../webviews/frame';
import loadOverlay from '../webviews/overlay';

class PlayerWindow {
  constructor(context, layout) {
    const { position } = context.getConfig();

    this.wid = layout?.wid || v4();
    this.context = context;
    this.elements = {
      win: new BrowserWindow({
        title: 'Split Player',
        fullscreen: false,
        transparent: false,
        frame: true,
        alwaysOnTop: false,
        show: false,
        backgroundColor: '#222',
        darkTheme: true,
        x: layout?.position?.x || position.left,
        y: layout?.position?.y || position.top,
        height: layout?.size?.height || 900,
        width: layout?.size?.width || 1500,
        webPreferences: {
          nodeIntegration: true,
          contextIsolation: true,
        },
      }),
      overlay: loadOverlay(),
      devtools: null,
      webviews: {},
    };
    this.displaySidebar = true;
    this.displayGrid = false;
    this.displaySettings = false;
    this.displayFavorites = false;
    this.displaySelector = false;
    this.frames = {};

    this.elements.win.addBrowserView(this.elements.overlay);
    this.elements.overlay.webContents.on('did-finish-load', () => {
      this.sendTreeUpdate({ path: ['wid'], data: this.wid });
      this.sendConfig();
      this.sendFavorites();

      this.makeContextMenu();

      if (layout) {
        if (layout.isMaximized) {
          this.elements.win.maximize();
        }

        this.loadLayout(layout.frames);
      } else {
        this.elements.win.maximize();
      }
    });
    this.elements.win.on('close', () => {
      this.closeWindow();
      this.context.removeWindow(this.wid);
    });

    /**
     * Resizing
     */
    this.elements.win.on('show', () => this.resize());
    this.elements.win.on('moved', () => this.resize());
    this.elements.win.on('resize', () => this.resize());
    this.elements.win.on('maximize', () => this.resize());
    this.elements.win.on('unmaximize', () => this.resize());
    this.elements.win.on('minimize', () => this.resize());
    this.elements.win.on('restore', () => this.resize());
    this.elements.win.on('enter-full-screen', () => this.resize());
    this.elements.win.on('leave-full-screen', () => this.resize());
    this.elements.win.on('enter-html-full-screen', () => this.resize());
    this.elements.win.on('leave-html-full-screen', () => this.resize());

    this.elements.win.show();
  }

  makeContextMenu() {
    const template = [
      {
        label: 'File',
        submenu: [
          {
            label: 'New Window',
            click: () => this.context.openWindow(),
            accelerator: 'CommandOrControl+N',
          },
          { type: 'separator' },
          {
            label: 'Empty memory cache',
            click: () => this.context.emptyMemoryCache(),
            accelerator: 'CommandOrControl+R',
          },
          {
            label: 'Empty full cache',
            click: () => this.context.emptyFullCache(),
            accelerator: 'CommandOrControl+Shift+R',
          },
          { type: 'separator' },
          { label: 'Restart', click: () => this.context.restart() },
          { label: 'Save and Exit', click: () => this.context.saveExit() },
          { role: 'quit' },
        ],
      },
      {
        label: 'Window',
        submenu: [
          {
            label: 'Toggle Sidebar',
            click: () => this.toggleSidebar(),
            accelerator: 'F7',
          },
          {
            label: 'Toggle Grid',
            click: () => this.toggleGrid(),
            accelerator: 'F8',
          },
          {
            label: 'Toggle Settings',
            click: () => this.toggleSettings(),
            accelerator: 'F9',
          },
          {
            label: 'Toggle Favorites',
            click: () => this.toggleFavorites(),
            accelerator: 'F10',
          },
          { role: 'togglefullscreen' },
          { type: 'separator' },
          {
            label: 'Open Dev Tools',
            click: () => this.toggleDevtools({}),
            accelerator: 'F12',
          },
          { type: 'separator' },
          { role: 'close' },
        ],
      },
    ];

    this.elements.win.setMenu(Menu.buildFromTemplate(template));
  }

  sendTreeUpdate(updates) {
    this.elements.overlay.webContents.send(
      'tree-update',
      Array.isArray(updates) ? updates : [updates]
    );
  }

  sendFrameData({ fid, event, data }) {
    if (fid && this.elements.webviews[fid]) {
      this.elements.webviews[fid].webContents.send(event, data);
    } else if (!fid) {
      Object.keys(this.elements.webviews).forEach(id => {
        this.elements.webviews[id].webContents.send(event, data);
      });
    }
  }

  sendFrames() {
    this.sendTreeUpdate({ path: ['frames'], data: this.frames });
  }

  sendConfig() {
    this.sendTreeUpdate({ path: ['config'], data: this.context.getConfig() });
  }

  sendFavorites() {
    this.sendTreeUpdate({
      path: ['favorites'],
      data: this.context.getFavorites(),
    });
  }

  loadLayout(frames) {
    frames.forEach(frame => {
      this.frames[frame.fid] = {
        fid: frame.fid,
        url: frame.url,
        gridPosition: frame.gridPosition,
        position: {
          top: 0,
          left: 0,
          width: 1,
          height: 1,
        },
      };

      this.reloadView(frame.fid);
    });

    this.resizeFrames();
  }

  saveLayout() {
    const [x, y] = this.elements.win.getPosition();
    const [width, height] = this.elements.win.getSize();
    const isMaximized = this.elements.win.isMaximized();

    this.context.saveLayout(this.wid, {
      wid: this.wid,
      position: { x, y },
      size: { width, height },
      isMaximized,
      frames: map(this.frames, frame => ({
        fid: frame.fid,
        url: frame.url,
        gridPosition: frame.gridPosition,
      })),
    });
  }

  createFrame({ url, row, col, rowCount, colCount }) {
    const fid = v4();

    this.frames[fid] = {
      fid,
      url,
      gridPosition: this.getNewFramePosition(row, col, rowCount, colCount),
      position: {
        top: 0,
        left: 0,
        width: 1,
        height: 1,
      },
    };

    this.resizeFrames();
    this.reloadView(fid);
    this.saveLayout();
  }

  getNewFramePosition(row, col, rowCount, colCount) {
    const { grid } = this.context.getConfig();
    const heightOffset = grid.height / rowCount;
    const widthOffset = grid.width / colCount;
    const position = {
      top: row * heightOffset,
      left: col * widthOffset,
      bottom: (row + 1) * heightOffset - 1,
      right: (col + 1) * widthOffset - 1,
    };

    return position;
  }

  moveFrame({ fid, gridPosition }) {
    if (!this.frames[fid]) return;

    this.frames[fid].gridPosition = gridPosition;
    this.resizeFrames();
    this.saveLayout();
  }

  updateFrame({ fid, url, skipReload }) {
    if (!this.frames[fid]) return;

    this.frames[fid].url = url;

    if (!skipReload) {
      this.reloadView(fid);
    }

    this.sendFrames();
    this.saveLayout();
  }

  reloadFrame({ fid }) {
    if (!this.frames[fid]) return;

    this.reloadView(fid);
  }

  swapFrames({ fid1, fid2 }) {
    if (!this.frames[fid1] || !this.frames[fid2]) return;

    const gridPosition = { ...this.frames[fid1].gridPosition };

    this.frames[fid1].gridPosition = { ...this.frames[fid2].gridPosition };
    this.frames[fid2].gridPosition = gridPosition;

    this.resizeFrames();
    this.saveLayout();
  }

  removeFrame({ fid }) {
    if (!this.frames[fid]) return;

    this.frames = omit(this.frames, fid);

    if (this.elements.webviews[fid]) {
      this.elements.webviews[fid].webContents.destroy();
      this.elements.win.removeBrowserView(this.elements.webviews[fid]);
      this.elements.webviews[fid] = null;
    }

    this.sendFrames();
    this.saveLayout();
  }

  toggleSidebar() {
    this.displaySidebar = !this.displaySidebar;
    this.sendTreeUpdate({
      path: ['displaySidebar'],
      data: this.displaySidebar,
    });
    this.resize();
  }

  toggleDisplays(key, data) {
    this[key] = data || !this[key];

    [
      'displayGrid',
      'displaySettings',
      'displayFavorites',
      'displaySelector',
    ].forEach(k => {
      if (k !== key) {
        this[k] = false;
      }

      this.sendTreeUpdate({
        path: [k],
        data: this[k],
      });
    });

    this.resizeOverlay();
  }

  toggleGrid() {
    this.toggleDisplays('displayGrid');
  }

  toggleSettings() {
    this.toggleDisplays('displaySettings');
  }

  toggleFavorites({ fid } = {}) {
    this.toggleDisplays('displayFavorites', fid);
  }

  toggleSelector({ url } = {}) {
    this.toggleDisplays(
      'displaySelector',
      url ? { url } : null
    );
  }

  toggleDevtools({ fid }) {
    let el;

    if (this.elements.devtools && !this.elements.devtools.isDestroyed()) {
      this.elements.devtools.destroy();
      this.elements.devtools = null;
    }

    if (!fid) {
      el = this.elements.overlay;
    } else if (this.elements.webviews[fid]) {
      el = this.elements.webviews[fid];
    }

    if (el) {
      this.elements.devtools = new BrowserWindow({
        title: 'Dev Tools',
        fullscreen: false,
        transparent: false,
        frame: true,
        alwaysOnTop: false,
        x: 2000,
        y: 0,
        height: 1000,
        width: 1500,
        show: true,
        webPreferences: {
          nodeIntegration: true,
        },
      });

      el.webContents.setDevToolsWebContents(this.elements.devtools.webContents);
      el.webContents.openDevTools({ mode: 'detach' });

      this.elements.devtools.on('close', () => {
        this.elements.devtools.destroy();
        this.elements.devtools = null;
      });
    }
  }

  resizeFrames() {
    const [width, height] = this.elements.win.getContentSize();
    const { grid, sideBarWidth } = this.context.getConfig();
    const sidebar = this.displaySidebar ? sideBarWidth : 0;
    const widthUnit = (width - sidebar) / grid.width;
    const heightUnit = height / grid.height;

    forEach(this.frames, ({ fid, gridPosition }) => {
      const newPos = {
        top: Math.floor(gridPosition.top * heightUnit),
        left: sidebar + Math.floor(gridPosition.left * widthUnit),
        width: Math.ceil(
          (gridPosition.right - gridPosition.left + 1) * widthUnit - 1
        ),
        height: Math.ceil(
          (gridPosition.bottom - gridPosition.top + 1) * heightUnit - 1
        ),
      };

      this.frames[fid].position = newPos;

      if (this.elements.webviews[fid]) {
        this.elements.webviews[fid].setBounds({
          x: newPos.left,
          y: newPos.top,
          width: newPos.width,
          height: newPos.height,
        });
        this.elements.webviews[fid].setBounds({
          x: newPos.left,
          y: newPos.top,
          width: newPos.width,
          height: newPos.height,
        });
      }
    });

    this.sendFrames();
  }

  resizeOverlay() {
    const [width, height] = this.elements.win.getContentSize();
    const { sideBarWidth } = this.context.getConfig();
    const overlayOpened = this.displayGrid
      || this.displaySettings
      || this.displayFavorites
      || this.displaySelector;

    if (overlayOpened) {
      this.elements.overlay.setBounds({
        x: 0,
        y: 0,
        width,
        height,
      });
      this.elements.overlay.setAutoResize({ width: true, height: true });
    } else {
      this.elements.overlay.setBounds({
        x: 0,
        y: 0,
        width: this.displaySidebar ? sideBarWidth : 0,
        height,
      });
      this.elements.overlay.setAutoResize({ width: false, height: false });
    }
  }

  resize() {
    setTimeout(() => {
      this.resizeOverlay();
      this.resizeFrames();
      this.saveLayout();
    }, 0);
  }

  reloadView(fid) {
    const { url, position } = this.frames[fid] || {};

    if (this.elements.webviews[fid]) {
      this.elements.webviews[fid].webContents.destroy();
      this.elements.win.removeBrowserView(this.elements.webviews[fid]);
      this.elements.webviews[fid] = null;
    }

    if (url) {
      this.elements.webviews[fid] = loadFrame({
        wid: this.wid,
        fid,
        url,
        scripts: this.context.getScripts(),
        updateURL: newUrl => this.updateFrame({
          fid,
          url: newUrl,
          skipReload: true,
        }),
      });
      this.elements.win.addBrowserView(this.elements.webviews[fid]);
      this.elements.webviews[fid].setBounds({
        x: position.left,
        y: position.top,
        width: position.width,
        height: position.height,
      });

      this.elements.win.removeBrowserView(this.elements.overlay);
      this.elements.win.addBrowserView(this.elements.overlay);
    }
  }

  closeWindow() {
    if (this.elements.devtools && !this.elements.devtools.isDestroyed()) {
      this.elements.devtools.destroy();
    }

    if (this.elements.win && !this.elements.win.isDestroyed()) {
      forEach(this.elements.webviews, view => {
        if (view) {
          view.webContents.destroy();
        }
      });

      this.elements.win.destroy();
    }

    this.elements = null;
  }
}

export default PlayerWindow;
